from __future__ import print_function, division

import math

__all__ = ['calculate_mpi_worker_parameters',
           'calculate_non_mpi_worker_parameters',
           'QDOLaunchException']

class QDOLaunchException(Exception):
    pass

def calculate_non_mpi_worker_parameters(nworkers,
                                        cores_per_worker,
                                        numa_pack,
                                        cores_per_node,
                                        cores_per_numa):
    """ Calculate launch internal parameters for non MPI workers running
    inside the same job: divide equally cores_per_worker among nworkers.
    
    Args:
        nworkers: number of workers in the same MPI domain.
        cores_per_worker: cores required for the tasks to be processed by
            the workers
        numa_pack: If true, workers will be configured so they do not
            allocate cores in different numas.
        cores_per_node: cores that node have (system parameter)
        cores_per_numa: cores that each NUMA have (system parameter)
        
    Returns:
        workers_per_node: workers to run on a single node
        workers_per_numa: if numa_pac==True number of workers to run on a
            single NUMA. None otherwise.
    """
    if (cores_per_worker<cores_per_node and 
            (cores_per_node % cores_per_worker !=0)):
        print("Warning: cores_per_node is not multiple of cores_per_worker. "
              "this may reduce the resource utilization")
        
    numas_per_node = cores_per_node // cores_per_numa
    if cores_per_worker<=0 or cores_per_worker>cores_per_node:
        print("Non MPI workers (mpi_nproc=0) "
                                 " must allocate "
                                 " less/equal number of cores than a" 
                                 " node's capacity: " +
                                 str(cores_per_node)) 
    if numa_pack and cores_per_worker > cores_per_numa:
        raise QDOLaunchException("Non MPI workers (mpi_nproc=0) with NUMA "
                                 " packing must allocate "
                                 " less/equal number of cores than a" 
                                 " NUMA's capacity: " +
                                 str(cores_per_node)) 
    
    workers_per_numa = None
    if numa_pack:
        workers_per_numa = cores_per_numa // cores_per_worker
        if workers_per_numa == 0:
            workers_per_numa = 1
        workers_per_node = numas_per_node * workers_per_numa
    else:
        workers_per_node = cores_per_node // cores_per_worker
        if workers_per_node == 0:
            workers_per_node = 1
        
    total_nodes = _div_rup(nworkers, workers_per_node)
    
    total_cores = cores_per_node * total_nodes
    
    # Make sure that we don't have more workers per node than workers in 
    # total. Aprun requirement.
    workers_per_node, workers_per_numa = _adjust_pe_limits(
                                    nworkers, workers_per_node,
                                    workers_per_numa,
                                    cores_per_node//cores_per_numa,
                                    numa_pack)
            
    return total_cores, \
            workers_per_node, workers_per_numa 
    

def calculate_mpi_worker_parameters(mpiproc_per_worker, cores_per_mpiproc,
                                    cores_per_worker,
                                    numa_pack, cores_per_node,
                                    cores_per_numa):
    """
    Calculate the internal parameters of the workers that require isolated
    MPI domains under three cases defined for launch. Check cases:
    - _calculate_mpi_case1
    - _calculate_mpi_case2
    - _calculate_mpi_case3
     
    """
    
    if cores_per_mpiproc!=None and cores_per_mpiproc<=0:
        raise QDOLaunchException("mpidepth cannot be <=0: " + 
                                 str(cores_per_mpiproc))
    if cores_per_worker!=None and cores_per_worker<=0:
        raise QDOLaunchException("cores_per_worker cannot be <=0: " + 
                                 str(cores_per_mpiproc))
    if cores_per_mpiproc is None and cores_per_worker is None:
        raise QDOLaunchException("At least one of mpidepth or cores_per_mpiproc"
                                 " has to be set.")
    if (cores_per_mpiproc!=None and cores_per_mpiproc > cores_per_numa and 
        numa_pack):
        raise QDOLaunchException("NUMA Pack activated but requester PEs "
                                 "allocate more cores than NUMA capacity: "
                                 +str(cores_per_mpiproc))
    if cores_per_mpiproc!=None and cores_per_mpiproc > cores_per_node:
        raise QDOLaunchException("PEs cannot allocate more cores than a "
                                 "single node capacity: "
                                 +str(cores_per_mpiproc))
    
    if cores_per_worker!=None and (
                        cores_per_worker < cores_per_node or 
                        cores_per_worker % cores_per_node!=0):
        raise QDOLaunchException("Requested cores ("+str(cores_per_worker)+
                                 ") per worker must be "
                                 "multiple of the number of cores per node"
                                 " "+str(cores_per_node))
    
    if cores_per_worker is None:
        cores_per_worker, mpiproc_per_node, mpiproc_per_numa = \
            _calculate_mpi_case1(mpiproc_per_worker,
                                      cores_per_mpiproc,
                                      numa_pack, cores_per_node,
                                      cores_per_numa)
    elif cores_per_mpiproc is None:
        cores_per_mpiproc, mpiproc_per_node, mpiproc_per_numa = \
            _calculate_mpi_case2(mpiproc_per_worker,
                                      cores_per_worker,
                                      numa_pack, cores_per_node,
                                      cores_per_numa)
            
    else:
        mpiproc_per_node, mpiproc_per_numa = \
            _calculate_mpi_case3( 
                             mpiproc_per_worker,
                             cores_per_worker,
                             cores_per_mpiproc,
                             numa_pack, cores_per_node,
                             cores_per_numa)
        
                    
      
    if numa_pack and cores_per_mpiproc > cores_per_numa:
        raise QDOLaunchException("NUMA pack activated and provided "
                                 "cores_per_mpiproc or resulting of provided "
                                 "cores_per_worker and mpiproc_per_worker "
                                 "forces PE to allocate more cores than"
                                 " than NUMA capacity")
    if not numa_pack and cores_per_mpiproc > cores_per_node:
        raise QDOLaunchException("provided cores_per_mpiproc or resulting of "
                                 " provided "
                                 "cores_per_worker and mpiproc_per_worker "
                                 "forces PE to allocate more cores than"
                                 " than a single NODE capacity")
    
    # Make sure that there are no more PEs per node than the total number
    # of PEs. This is an aprun requirement.
    mpiproc_per_node, mpiproc_per_numa = _adjust_pe_limits(
                                    mpiproc_per_worker, mpiproc_per_node,
                                    mpiproc_per_numa,
                                    cores_per_node//cores_per_numa,
                                    numa_pack)

    return cores_per_worker, mpiproc_per_worker, cores_per_mpiproc, \
            mpiproc_per_node, mpiproc_per_numa 

def _adjust_pe_limits(mpiproc_per_worker, mpiproc_per_node, mpiproc_per_numa,
                      numas_per_node, numa_pack):
    # You cannot run more PEs per node than total PEs
    if mpiproc_per_node>mpiproc_per_worker:
        mpiproc_per_node = min(mpiproc_per_node, mpiproc_per_worker)
        if numa_pack:
            mpiproc_per_numa = _div_rup(mpiproc_per_node, numas_per_node)
            # reduction may set mpiproc_per_numa to 0
            mpiproc_per_numa = max(mpiproc_per_numa, 1)
    
    return mpiproc_per_node, mpiproc_per_numa    

def _calculate_mpi_case1(
                         mpiproc_per_worker,
                         cores_per_mpiproc,
                         numa_pack, cores_per_node,
                         cores_per_numa):
    """
    Calculate the required cores, PEs per node and PEs per numa for a worker
    that requires mpiproc_per_worker PEs and each one of them use
    cores_per_mpiproc cores. It takes into account fragmentation: a worker
    cannot use cores of different nodes or NUMAS (if numa_pack = True)
    
    Args:
        mpiproc_per_worker: PEs used by each worker
        cores_per_mpiproc: cores required by each pe.
        numa_pack: if True, worker PEs will not use cores from different
            NUMAS.
        cores_per_node: cores that node have (system parameter)
        cores_per_numa: cores that each NUMA have (system parameter)
        
    Returns:
        cores_per_worker: resulting number of cores required by the worker
        mpiproc_per_node: PEs to be run in each node.
        mpiproc_per_numa: If numa_pack is true: number of PEs in each NUMA.
            None otherwise.    
    """
    
    
    if not numa_pack:
        mpiproc_per_node = cores_per_node // cores_per_mpiproc
        mpiproc_per_numa = None
    else:
        mpiproc_per_numa = cores_per_numa // cores_per_mpiproc
        numas_per_node = cores_per_node // cores_per_numa
        mpiproc_per_node = mpiproc_per_numa*numas_per_node
    nodes_per_worker = _div_rup(mpiproc_per_worker, mpiproc_per_node)
    cores_per_worker = nodes_per_worker * cores_per_node
       
    return cores_per_worker, mpiproc_per_node, mpiproc_per_numa 
    
    
def _calculate_mpi_case2( 
                         mpiproc_per_worker,
                         cores_per_worker,
                         numa_pack, cores_per_node,
                         cores_per_numa):
    """
    Calculate the cores per PE needed to run a worker that requires
    cores_per_worker cores and mpiproc_per_worker PEs. 
    
    Args:
        mpiproc_per_worker: PEs that the worker require.
        cores_per_worker: Number of cores that the worker must allocate.
            It has to be multiple of cores_per_node
        numa_pack: if True, worker PEs will not use cores from different
            NUMAS.
        cores_per_node: cores that node have (system parameter)
        cores_per_numa: cores that each NUMA have (system parameter)
    Returns: 
        cores_per_mpiproc: cores allocated by each PE. 
        mpiproc_per_node: PEs per node
        mpiproc_per_numa: If numa_pack is true: number of PEs in each NUMA.
            None otherwise. 
    """

    if not numa_pack:
        nodes_per_worker =  cores_per_worker // cores_per_node
        mpiproc_per_node = _div_rup(mpiproc_per_worker, nodes_per_worker)
        cores_per_mpiproc = cores_per_node // mpiproc_per_node
        mpiproc_per_numa = None
    else:
        nodes_per_worker = cores_per_worker // cores_per_node
        numas_per_node = cores_per_node//cores_per_numa
        numas_per_worker = numas_per_node * nodes_per_worker
        mpiproc_per_numa = _div_rup(mpiproc_per_worker, numas_per_worker)
        mpiproc_per_node = mpiproc_per_numa * numas_per_node
        cores_per_mpiproc = cores_per_numa // mpiproc_per_numa
    return cores_per_mpiproc, mpiproc_per_node, mpiproc_per_numa 

def _calculate_mpi_case3( 
                         mpiproc_per_worker,
                         cores_per_worker,
                         cores_per_mpiproc,
                         numa_pack, cores_per_node,
                         cores_per_numa):
    """
    Calculate the PEs per node and PEs per numa for a worker
    that requires mpiproc_per_worker PEs and each one of them use
    cores_per_mpiproc cores. It takes into account fragmentation: a worker
    cannot use cores of different nodes or NUMAS (if numa_pack = True).
    
    Args:
        mpiproc_per_worker: PEs used by each worker
        cores_per_mpiproc: minimum number of cores required by each PE
        cores_per_worker: cores to be allocated 
        numa_pack: if True, worker PEs will not use cores from different
            NUMAS.
        cores_per_node: cores that node have (system parameter)
        cores_per_numa: cores that each NUMA have (system parameter)
        
    Returns:
        mpiproc_per_node: PEs to be run in each node.
        mpiproc_per_numa: If numa_pack is true: number of PEs in each NUMA.
            None otherwise.    
    """
    
    if not numa_pack:
        total_nodes = cores_per_worker // cores_per_node
        mpiproc_per_numa = None
        mpiproc_per_node = _div_rup(mpiproc_per_worker, total_nodes)
        
    else:
        total_numas = cores_per_worker // cores_per_numa
        mpiproc_per_numa = _div_rup(mpiproc_per_worker, total_numas)
        mpiproc_per_node = mpiproc_per_numa * (cores_per_node//cores_per_numa)
    

    
    if cores_per_node//mpiproc_per_node < cores_per_mpiproc:
            raise QDOLaunchException("Input parameters induce less cores "
                                     "per PE than"
                                     "requested: " +
                                     str(cores_per_node//mpiproc_per_node))
            
    return mpiproc_per_node, mpiproc_per_numa 
    

def _div_rup(up, down):
    """returns the round up division of two integers."""
    return int(math.ceil(float(up)/float(down)))
