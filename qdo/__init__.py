"""
Lightweight infrastructure to process many little tasks in a queue.

QDO (kew-doo) is a toolkit for managing many many small tasks
within a larger batch framework.  QDO separates the queue of tasks
to perform from the batch jobs that actually perform the tasks.
This simplifies managing tasks as a group, and provides greater
flexibility for scaling batch worker jobs up and down or adding additional
tasks to the queue even after workers have started processing them.

The qdo module provides an API for interacting with task queues.
The qdo script uses this same API to provide a command line interface
that can be used interchangeably with the python API.
Run "qdo -h" to see the task line options.

Example:

#- Load queue
import qdo
q = qdo.create('EchoChamber')
for i in range(100):
    q.add('echo '+str(i))

#- Launch 10 batch jobs to process these tasks
q.launch(10)

#- The worker jobs are basically doing this:
import qdo
q = qdo.connect('EchoChamber')
q.do()

Stephen Bailey, Fall 2013
"""

from .core import __version__
from .core import *
from .launch_calc import *

# TODO: delay spawning backend connections/sanity checks on import.
