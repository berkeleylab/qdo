from __future__ import print_function, division

import sys
import os
import subprocess
import signal
import time
import json   #- For encoding/decoding objects as strings
import shutil
import uuid   #- For unique message IDs
import math
#- Use ConfigParser to parse etc/batch_config.ini
if sys.version_info[0] == 2:
    from ConfigParser import SafeConfigParser as ConfigParser, NoSectionError
elif sys.version_info[0] == 3:
    from configparser import ConfigParser, NoSectionError
from pkgutil import get_loader  #- to find files included in the package
from collections import Counter #- To count tasks of each state in a timestamp

from .launch_calc import (calculate_mpi_worker_parameters,
                          calculate_non_mpi_worker_parameters,
                          QDOLaunchException)

if sys.version_info[0] == 2:
    string_types = basestring
elif sys.version_info[0] == 3:
    string_types = str

__all__ = ["Task", "Queue", "Backend", "create", "connect", "queues", "qlist",
           "print_queues", "valid_queue_name", "tojson", "set_project"]

__version__ = "0.8.dev1"


# For handling Slurm jobs that time out and are killed by SIGTERM:
# a global flag + accessor
_terminated = False
def set_terminated():
    global _terminated
    _terminated = True
def is_terminated():
    return _terminated


#-------------------------------------------------------------------------
#- Lightweight wrapper class for tasks
        
class Task(object):
    """
    Wrapper class for a single qdo task
    """
    #- Enumeration of various states a Task could be in
    ### BLOCKED   = 'Blocked'       #- A required dependency failed
    WAITING   = 'Waiting'       #- Waiting for dependencies to finish
    PENDING   = 'Pending'       #- Eligible to be selected and run
    RUNNING   = 'Running'       #- Running right now
    SUCCEEDED = 'Succeeded'     #- Finished with err code = 0
    FAILED    = 'Failed'        #- Finished with err code != 0
    KILLED    = 'Killed'        #- Finished with SIGTERM (eg Slurm job timeout)
    UNKNOWN   = 'Unknown'       #- Something went wrong and we lost track
    VALID_STATES = [WAITING, PENDING, RUNNING, SUCCEEDED, FAILED, KILLED]

    def __init__(self, task, queue, id=None, state=UNKNOWN, err=None, message=None, jobid=None, priority=None, **kwargs):
        """
        Create Task object
        
        Required:
        task    : input string or JSON-able object
        Either `queue` or `queuename`:
            queue     : parent Queue that this task is associated with
            queuename : name of queue to use
        
        Optional:
        id      : unique id string to identify this task;
                  default will use uuid4() to generate unique id.
        state   : one of the states in Task.VALID_STATES or Task.UNKNOWN
        err     : integer error code
        message : message string
        
        kwargs are ignored, allowing Tasks to be created from dictionaries
        with more keys than strictly necessary
        """
        if id is None:
            self.id = str(uuid.uuid4())
        else:
            self.id = id
            
        self.task = task
        if isinstance(queue, Queue):
            self.queue = queue
        else:
            self.queue = connect(queue)

        self.priority = priority
        self.state = state
        self.err = err
        self.message = message
        self.jobid = jobid

    #- Implement iterator to allow conversion x = dict(task)
    #- Convert back to Task with t = Task(**x)
    def __iter__(self):
        yield 'task', self.task
        yield 'id', self.id
        yield 'state', self.state
        yield 'err', self.err
        yield 'message', self.message
        yield 'queuename', self.queue.name

    def __repr__(self):
        return "<qdo.Task {}>".format(dict(self))

    def run(self, quiet=False, script=None, func=None, prefix=None):
        """
        Treat task as a command to run via subprocess.call()       

        If *script* is given, it is prefixed to the task or used as a
        template string to format tasks that are lists or dictionaries.
        
        If *func* is given, call func(task) instead of spawning a command.
        
        If *prefix* is given, it is prefix to the task, before the script.

        Return resulting error code (0=success)
        """
        if not quiet:
            print("## -----------------------------------------------------------------------")
            # print("## QDO Starting %s  %s (%s)" % (self.id, time.asctime(), time.time()))
            print("## QDO Starting %s  %s  %s" % (self.id, time.asctime(), self.task))
            print("## QDO Task:", self.task)
            if script is not None:
                print("## QDO Task script:", script)

        sys.stdout.flush()
        sys.stderr.flush()
        
        #- Be optimistic
        err = 0
        message = None

        #- Should we pass the task to a python function?
        if func is not None:
            try:
                result = func(self.task)
                if result is not None:
                    message = str(result)
            except Exception as e:
                print(e)
                message = e.message
                err = 42
                
        #- Otherwise try spawning a subprocess.call()
        else:
            try:
                cmd = self.task
                if script is not None:
                    try:
                        if isinstance(cmd, string_types):
                            cmd = script + ' ' + cmd
                        elif isinstance(cmd, dict):
                            cmd = script.format(**cmd)
                        elif isinstance(cmd, (list, tuple)):
                            cmd = script.format(*cmd)
                    except:
                        sys.stderr.write("ERROR: failed to combine script with args")
                        err = 1
                if prefix is not None: 
                    cmd = prefix + ' ' + cmd
                        

                #- No parsing failures, so let's try to run it
                if err == 0:
                    if isinstance(cmd, string_types):
                        env_vars=None
                        backedup_path=os.getenv("QDO_BACKEDUP_PATH", None)
                        if backedup_path:
                            env_vars=dict(os.environ)
                            env_vars["PATH"]=backedup_path
                        proc = subprocess.Popen(cmd, shell=True, env=env_vars)
                        # We poll() the subprocess to give the Python runtime
                        # an opportunity to deliver signals (eg, SIGTERM).
                        while True:
                            r = proc.poll()
                            if r is not None:
                                err = r
                                break
                            if is_terminated():
                                #print('Task.run received is_terminated.')
                                # send SIGTERM to child??
                                proc.terminate()
                                err = -signal.SIGTERM
                                message = 'Batch job timed out'
                                time.sleep(5)
                                # send SIGKILL to child??
                                proc.kill()
                                break
                            time.sleep(1)
                    else:
                        sys.stderr.write("ERROR: task is not a string or args to a script")
                        err = 126

            except OSError as oserr:
                err = oserr.errno
            except:
                import traceback
                print('Exception during Task.run:')
                traceback.print_exc()

        #- Wrap up the state of what just happened
        sys.stdout.flush()
        sys.stderr.flush()
        if err == 0:
            self.set_state(Task.SUCCEEDED, err=err, message=message)
            if not quiet:
                print("## QDO SUCCESS  %s  %s  %s" % (self.id, time.asctime(), self.task))
        elif err == -signal.SIGTERM:
            self.set_state(Task.KILLED, err=err, message=message)
            if not quiet:
                print("## QDO KILLED   %s  %s  %s" % (self.id, time.asctime(), self.task))
                print("## QDO Error Code", err)
        else:
            self.set_state(Task.FAILED, err=err, message=message)
            if not quiet:
                print("## QDO FAILED   %s  %s  %s" % (self.id, time.asctime(), self.task))
                print("## QDO Error Code", err)

        if not quiet:
            print()

        return err

    def set_state(self, state, err=None, message=None):
        """
        Set the state of this task.
        
        `state` can be any string, but generally should be one of the values 
        encoded with Task.VALID_STATES.
        
        Optionally provide integer err code and/or message string.
        """
        self.state = state
        self.err = err
        self.message = message
        self.queue.set_task_state(self.id, state, err=err, message=message)

#-------------------------------------------------------------------------
#- The primary Queue class.  Specific backends subclass this and implement
#- the _add(), _get(), etc. methods to provide the functionality

class Queue(object):
    """Definition of Queue interface to be implemented by various backends"""
    
    #- Enumeration of various states a Queue could be in
    ACTIVE  = 'Active'
    PAUSED  = 'Paused'
    UNKNOWN = 'Unknown'
    VALID_STATES = [ACTIVE, PAUSED]
    
    def __init__(self):
        """
        Queue object for managing tasks independent of batch jobs.
        
        Users should get queue objects via the .create() and .connect()
        methods of qdo or specific Backend objects rather than directly
        create them.
        """
        self.set_state_wall_total = 0.

    def __str__(self):
        count = self.status()['ntasks']
        result = '{} is {}'.format(self.name, self.state)
        for state in Task.VALID_STATES:
            result += '\n{:10s} : {}'.format(state, count[state])
        return result

    #- Implement iterator to allow conversion x = dict(queue)
    def __iter__(self):
        s = self.status()
        yield 'name', s['name']
        yield 'ntasks', s['ntasks']
        yield 'state', s['state']
        yield 'user', s['user']
        
    def __repr__(self):
        return "<qdo.Queue {}>".format(dict(self))

    def loadfile(self, filename, priority=None):
        """
        Load tasks from a file, one per line
        
        Blank lines and lines starting with # comments are ignored.
        Trailing # comment strings are passed through as part of the
        task string.
        
        if filename == '-' then read from stdin instead.
        
        Optional priority [float] is applied to all tasks in this file.
        """
        tasks = list()
        if filename == '-':
            f = sys.stdin
        else:
            f = open(filename)
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            elif line.startswith('#'):
                continue
            else:
                tasks.append(line)

        if f is not sys.stdin:
            f.close()

        priorities = None
        if priority is not None:
            priorities = [priority] * len(tasks)

        return self.add_multiple(tasks, priorities=priorities)
                
    def add(self, task, id=None, priority=None, requires=None):
        """
        Add a single task to the queue
        
        task : any string or object that can be converted into a string
               using json.dumps()

        Optional inputs:
            id       : unique id; if None, then uuid4() will be used
            priority : float.  Higher priorities run first.
            requires : list of task ids that must finish before this runs
            
        Notes on requires:
        Dependencies must finish but they don't have to succeed.
            Failing is still finishing.
        
        Returns taskid added
        """
        return self._add(task, id=id, priority=priority, requires=requires)

    def add_multiple(self, tasks, ids=None, priorities=None, requires=None):
        """
        Add multiple tasks to a queue; see add()
    
        Inputs:
            tasks : array of tasks to add
        
        Optional inputs:
            ids   : array of same length as tasks[] of unique ids
            priorities : array of priorities; higher priorities run first
            requires : array of tuples of tasks upon which this entry
                       depends.  Use None or empty tuple for entries
                       without dependencies.
                       
        Returns list of taskids added.
        """
        return self._add_multiple(tasks, ids=ids, priorities=priorities, requires=requires)

    def set_task_state(self, taskid, state, err=None, message=None):
        """
        Update task to given state (Task.SUCCEDED, Task.FAILED, ...)
        
        Optionally log integer error code and message string.
        """
        t1 = time.time()
        self._set_task_state(taskid, state, err=err, message=message)
        t2 = time.time()
        self.set_state_wall_total += (t2 - t1)

    def get(self, timeout=60, jobid=None):
        """
        Get a task from the queue and flag as running.
        
        Returns Task object
        
        If no tasks are available within timeout seconds
        or queue is paused, return None
        """
        return self._get(timeout=timeout, jobid=jobid)
        
    pop = get
    """pop() is the same as get()"""

    def retry(self, state=Task.FAILED, exitcode=None, priority=None, taskfilter=None):
        """
        Move tasks in `state` (e.g. Task.FAILED) back into pending

        Returns list of taskIDs moved back to pending
        """        
        #- WARNING: depending upon the backend implementation, this could
        #- leave queue/db in an inconsistent state if it fails in the middle.
        return self._retry(state=state, exitcode=exitcode, priority=priority,
                           taskfilter=taskfilter)

    def rerun(self, priority=None):
        """
        Move all completed tasks back into pending (succeeded AND failed)
        
        If exitcode is given, rerun only tasks that exited with that code.
        
        Returns list of taskIDs moved back to pending
        """
        #- WARNING: depending upon the backend implementation, this could
        #- leave queue/db in an inconsistent state if it fails in the middle.
        return self._rerun(priority=priority)

    def recover(self, priority=None, taskfilter=None):
        """
        Recover from mid-task batch job failure by moving running tasks
        back into pending.

        Returns list of taskIDs moved back to pending
        """
        return self.retry(state=Task.RUNNING, priority=priority, taskfilter=taskfilter)

    def revive(self, priority=None, taskfilter=None):
        """
        Recover "Killed" jobs by moving them back into pending.

        Returns list of taskIDs moved back to pending
        """
        return self.retry(state=Task.KILLED, priority=priority, taskfilter=taskfilter)

    def do(self, timeout=60, quiet=False, runtime=None, script=None, func=None,
           prefix=None, jobid=None):
        """
        Process tasks from queue until no tasks are available for
        timeout seconds.
        
        If *runtime* is given, only process tasks for runtime seconds.

        If *func* is given, pass task to this python fuction.
        
        If *script* is given, run that script with the queued task
        as argument.
        
        If *prefix* is give, append it just before the queued task and the
        script.

        If *jobid* (string) is given, that column in the Task database
        will be set.

        quiet=True will be less chatty.
        
        Returns dictionary with task_count, time elapsed getting tasks,
        and time elapsed running tasks
        """
        n = 0
        time_get = 0.0
        time_run = 0.0
        tstart = time.time()
        set_state_wall_0 = self.set_state_wall_total
        while True:
            t1 = time.time()
            if runtime is not None and t1-tstart > runtime:
                print('## QDO QUITTING: runtime %.1f > %.1f' % (t1-tstart, runtime))
                break
            if is_terminated():
                print('## QDO TERMINATED')
                break
            task = self.get(timeout=timeout, jobid=jobid)
            t2 = time.time()
            time_get += (t2 - t1)
            if task is None:
                break  #- no task means we are finished
            else:
                try:
                    err = task.run(quiet=quiet, script=script, func=func,
                                   prefix=prefix)
                    t3 = time.time()
                except KeyboardInterrupt as e:
                    t3 = time.time()
                    task.set_state(Task.FAILED, err=130, message=repr(e))
                    print("Keyboard Interrupt -- stopping")
                    print(self)
                    sys.exit(130)
                except Exception as e:
                    t3 = time.time()
                    task.set_state(Task.FAILED, err=99, message=repr(e))

                time_run += (t3 - t2)
                n += 1

            tset = self.set_state_wall_total - set_state_wall_0
            print("### QDO Task: %.3f s overhead / %.3f s task : %s" %
                  (tset + (t2-t1), t3-t2, task.task))

        return {'task_count': n, 'elapsed_get': time_get, 'elapsed_run': time_run}
        # return n

    # run_tasks = do
    # ''' run_tasks() is an alias for do(). '''

    def status(self):
        """
        Return dictionary r describing the queue status
        
        r['state'] = queue state 'Active' or 'Paused'
        r['user'] = user who owns this queue
        r['name'] = name of the queue
        r['ntasks'] = dictionary d[state] = ntasks
            where state = Waiting, Pending, Running, Succeeded, Failed, or Killed
            and ntasks = number of tasks in that state        
        """
        results = dict(state=self.state, name=self.name, user=self.user)
        results['ntasks'] = self.tasks(summary=True)
        return results

    summary = status
    """summary() is a synonym for status()"""

    def print_task_state(self):
        """
        DEPRECATED: Print how many jobs are pending/running/succeeded/failed
        
        Printing the queue object itself will do the same thing
        """
        print(self)

    #- TODO: is this too much feature overloading based upon options?
    #- Maybe q.tasks(summary=True) should be q.ntasks()
    def tasks(self, id=None, state=None, summary=False, exitcode=None):
        """
        Return list of Task objects for this queue.

        if state is not None, return only tasks in that state
            (see Tasks.VALID_STATES)
            
        if id is not None, return just that Task object.
        raises ValueError if that id doesn't exist in the queue.

        if summary=True, return dictionary of how many tasks are in each state.
        
        if exitcode is not None and id is None, return only tasks that
        exited with that exit code.
        """
        return self._tasks(id=id, state=state, summary=summary, exitcode=exitcode)

    def print_tasks(self, verbose=False, state=None, exitcode=None):
        """
        Print tasks and their state, including succeeded/failed tasks
        
        if verbose=True, also print ID and error codes
        """
        if verbose:
            print('State       Err      ID                 Jobid    Priority   Task')
        else:
            print('State       Task')

        for x in self.tasks(state=state, exitcode=exitcode):
            if verbose:
                err = x.err if x.err is not None else '---'
                print('%-10s  %3s  %6s  %20s  %10s %s' % (x.state, err, x.id, x.jobid, x.priority, x.task))
            else:
                print('%-10s  %s' % (x.state, x.task))

    def delete(self):
        """
        Delete the backend queue
        """
        return self._delete()

    @property
    def state(self):
        """
        Return string describing queue state: Queue.ACTIVE or Queue.PAUSED
        """
        return self._state()

    def pause(self):
        """
        Pause queue to prevent further processing, but allow running tasks
        to cleanly finish.  New tasks won't be processed until resume() is
        called and batch jobs are resubmitted.
        """
        return self._pause()

    def resume(self):
        """
        Restart a paused queue; see Queue.pause()
        """
        return self._resume()

    def launch(self, nworkers, njobs = 1,
                   mpiproc_per_worker=0, cores_per_mpiproc=None, cores_per_worker=1,
                   numa_pack=False,
                   script=None, timeout=None, runtime=None, batchqueue=None,
                   walltime=None,
                   batch_opts='', verbose=False, export_all_env_vars=False,
                   extra_vars_to_export=[]):
        """
        Submit jobs to process tasks from queue

        nworkers    : number of workers to run
        njobs       : workers will be spread among njobs jobs
        mpiproc_per_worker : number of MPI Processing Elements (PE) present in a
                    task.
        cores_per_mpiproc: number of cores required by each PE in each task
        cores_per_worker: total number of cores required for the worker
        numa_pack   : if true PEs and non non-mpi workers won't be allocated
                      across NUMAS
        script      : treat tasks in queue as arguments for this script
        timeout     : timeout for qdo task polling [seconds]
        runtime     : how long to run jobs for [seconds]
        batchqueue  : batch queue to submit jobs to (diff from qdo queue name)
        walltime    : batch job walltime limit [hh:mm:ss]
        batch_opts  : extra option string to pass to job launcher
        export_all_env_vars : if true current environment varaibles are passed
                    the launched jobs. Only QDO related vars are copied if False
        extra_vars_to_export: list of extra environment variables to be passed
                    to launched jobs if export_all_env_vars==False (apart from
                    the QDO ones).
        
        The behavior of launch changes depending on its parameters:
        mpiproc_per_worker = 0 : launch will create a single job for nworkers
            workers each one using cores_per_worker cores, all running in the
            same MPI domain. mpiddepth is ignored. Each worker cannnot have more
            cores_per_worker than a single node capacity.
        mpiproc_per_worker >= 1 : launch will create a single job for nworkers
            workers. Each worker is an MPI job with mpiproc_per_worker PEs.
            Then, there are three possibilities:
            - To set cores_per_mpiproc (number of cores per PE). The resulting
              worker PEs will use cores_per_mpiproc cores. The number of cores
              per worker is calculated to use the smallest possible number of
              cores. 
            - To set cores_per_worker: The cores then are divided among the PEs
              cores_per_worker must be at least mpiproc_per_worker. It has to be
              multiple of the number of cores per node. It will ensure that the
              PEs receive the maximum possible number of cores (and
              proportionally other resources of the machine)
            - To set both: PEs use cores_per_workers but they are divided among
              the nodes equivalent to cores_per_worker.
        
        [qdo_package]/etc/batch_config.ini controls the batch
        configuration options. More information in the documentation.
        
        """
        if self.state != Queue.ACTIVE:
            err_msg = "ERROR: Queue %s is not active\n" % self.name
            err_msg += 'Run "qdo resume %s" first' % self.name
            raise QDOLaunchException(err_msg)
            
        config = _get_batch_config()
        
        #- Eventually this boils down to
        #- $QDO_MPIRUN $QDO_DIR/bin/qdo do $QDO_NAME $QDO_OPTS

        #- Make sure we know where to find qdo
        if 'QDO_DIR' not in os.environ:
            os.environ['QDO_DIR'] = _qdo_dir()

        #- Set name of qdo queue
        os.environ['QDO_NAME'] = self.name

        #- Construct options to pass to "qdo do" via $QDO_OPTS
        qdo_opts = list()
        
        if script is not None:
            text_script=script
        else:
            text_script = os.getenv("QDO_SCRIPT")
        if _check_valid(text_script):
            os.environ['QDO_SCRIPT'] = text_script

        if timeout is not None:
            text_timeout = str(timeout)
        else:
            text_timeout = os.getenv("QDO_TIMEOUT")
        if _check_valid(text_timeout):
            qdo_opts.extend(["--timeout", text_timeout])
         
        if runtime is not None:
            text_runtime = str(runtime)
        else:
            text_runtime = os.getenv("QDO_RUNTIME")
        if _check_valid(text_runtime):
            qdo_opts.extend(["--runtime", text_runtime])
            
        qdo_opts.extend(["--jitter", "{:.2f}".format(nworkers/4.0)])

        os.environ['QDO_OPTS'] = ' '.join(qdo_opts)

        #- Construct options to pass to job launcher
        if batch_opts is None:
            batch_opts = ''
        if walltime is not None:
            batch_opts += ' ' + config['walltime_opt'].format(walltime=walltime)

        cores_per_node = config['cores_per_node']
        cores_per_numa = int(config.get('cores_per_numa_node', cores_per_node))
        if nworkers % njobs!=0 :
            raise QDOLaunchException("Number of workers (%d) must be divisible "
                                     "by the number of requested job (%d)."%(
                                        nworkers, njobs))
        else:
            nworkers = nworkers//njobs

        hyperthreads = int(config.get('hyperthreads', 1))
        if verbose:
            print('Cores per node:', cores_per_node)
            print('Cores per NUMA node:', cores_per_numa)
            print('Hyperthreads per core:', hyperthreads)
        
        if batchqueue is not None:
            batch_opts += ' '+config['batchqueue_opt'].format(
                                                    batchqueue=batchqueue)
        # mpiproc_per_worker has to be valid
        if mpiproc_per_worker < 0 or mpiproc_per_worker is None:
            raise QDOLaunchException("mpiproc_per_worker cannot be negative or "
                                     "None: "+
                                      str(mpiproc_per_worker))
        elif mpiproc_per_worker == 0:
            if (cores_per_mpiproc!=None):
                raise QDOLaunchException("Cores per PE specified when MPI "
                                         "Processing Elements == 0")
            # NON MPI Cases. Traditional within a single node. One Apprun for
            # all the workers.
            if ('serial_queue' in config and
                    batchqueue == config['serial_queue']):
                cores_per_node = 1
                cores_per_numa = 1
            
            total_cores,  \
                mpiproc_per_node, mpiproc_per_numa =  \
                    calculate_non_mpi_worker_parameters(
                                         nworkers,
                                         cores_per_worker,
                                         numa_pack, cores_per_node,
                                         cores_per_numa)
            cores_per_mpiproc = cores_per_worker
        else:
            # The worker is an MPI job. For each worker we will do an apprun
            final_cores_per_worker, mpiproc_per_worker, cores_per_mpiproc, \
                mpiproc_per_node, mpiproc_per_numa = \
                    calculate_mpi_worker_parameters(
                                        mpiproc_per_worker, cores_per_mpiproc,
                                        cores_per_worker,
                                        numa_pack, cores_per_node,
                                        cores_per_numa)
            total_cores = final_cores_per_worker * nworkers
        os.environ['QDO_WORKERS_PER_JOB'] = str(nworkers)
    
        # Configuring QDO_MPIRTUN
        if (mpiproc_per_worker==0 and 'serial_queue' in config and 
                batchqueue == config['serial_queue']):
            os.environ['QDO_MPIRUN'] = ''
        else:
            if (mpiproc_per_worker==0):
                final_nPEs=nworkers
                final_cores_per_PE=cores_per_worker
            else:
                final_nPEs=mpiproc_per_worker
                final_cores_per_PE=cores_per_mpiproc
            hcores_per_PE = final_cores_per_PE * hyperthreads
            print('Hyperthreads per task:', hcores_per_PE)
            if numa_pack:
                os.environ['QDO_MPIRUN'] = config['mpirun'].format(
                    nPEs = final_nPEs,
                    PEs_per_node = mpiproc_per_node,
                    cores_per_PE = final_cores_per_PE,
                    hcores_per_PE = hcores_per_PE,
                    PEs_per_numa_node = mpiproc_per_numa)
            else:
                os.environ['QDO_MPIRUN'] = config['mpirun_no_numa'].format(
                    nPEs = final_nPEs,
                    PEs_per_node = mpiproc_per_node,
                    cores_per_PE = final_cores_per_PE,
                    hcores_per_PE = hcores_per_PE)
        if verbose:
            print('MPIRUN:', os.environ['QDO_MPIRUN'])

        # Time to do QSUB
        #- Options for constructing qsub command
        all_env_vars_argument = config['all_env_vars']
        var_list_argument = config['var_list']
        var_list_separator = config['var_list_separator']
        if export_all_env_vars:
            env_vars_argument=all_env_vars_argument
        else:
            total_list_of_vars=['QDO_MPIRUN', 'QDO_NAME',
                                                'QDO_OPTS',
                        'QDO_DIR', 'QDO_WORKERS_PER_JOB','QDO_SCRIPT',
                        'QDO_TIMEOUT','QDO_RUNTIME', 'QDO_OPTS', 'QDO_BACKEND',
                        'QDO_BATCH_PROFILE', 'QDO_DB_NAME', 'QDO_DB_HOST',
                        'QDO_DB_PORT', 'QDO_DB_USER', 'QDO_DB_PASS',
                        'QDO_DB_DIR', 'QDO_BATCH_CONFIG', 'QDO_AWS_REGION',
                        'QDO_TEST_DIR', 'QDORC_LOCATION', "OMP_NUM_THREADS"]
            #- Only pass set vars. Environment vars set to empy string brake
            #  brake programs.
            total_list_of_vars+=extra_vars_to_export
            final_list=[]
            for var_name in total_list_of_vars:
                if os.getenv(var_name) is not None:
                    final_list.append(var_name)
            string_list=var_list_separator.join(final_list)
            env_vars_argument=var_list_argument.format(name_list=string_list)
        
        
        qsub_opts = dict(
            ncores=total_cores,
            nodes=int((total_cores + cores_per_node-1) / cores_per_node),
            workers_per_node=mpiproc_per_node,
            cores_per_node=cores_per_node,
            qdo_dir = _qdo_package_dir(),
            jobname=self.name,
            batch_opts=batch_opts,
            env_vars=env_vars_argument)
        
        if "slurm" in config.keys():
            if mpiproc_per_worker==0:
                qsub_opts["nnodes"] = int(total_cores / cores_per_node)
            else:
                qsub_opts["nnodes"] = int(total_cores / cores_per_node)
                # In Case OMP is used, this env var controls the number of
                # threads per MPI PE.
                if os.getenv("OMP_NUM_THREADS", None) is None:
                    os.environ["OMP_NUM_THREADS"] = str(cores_per_mpiproc)
                if os.getenv("KMP_AFFINITY", None) is None:
                    os.environ["KMP_AFFINITY"] = "scatter"
                
        #- Finally ready to submit jobs!
        if mpiproc_per_worker == 0:
            qsub_command = 'qsub'
        else: 
            qsub_command = 'qsub_mpi'
        
        for job in range(njobs):
            cmd = config[qsub_command].format(**qsub_opts)
            print("Launching job ("+str(job+1)+"/"+str(njobs)+"): "+cmd)
            os.system(cmd)
    
    def events(self, epoch_start=None, epoch_end=None, state=None):
        """Returns a list of of tasks state changes for the tasks in the queue
        between epoch_start and epoch_end. 
        
        Args:
            epoch_start: long representing the minimum epoch time stamp of an
                event.
            epoct_end: long representing the maximum epoch time stamp of an 
                event.
            state: If not set, it returns all events. If set to a valid task
                state, returns only the events corresponding to that state.
        
        Returns: A list of dictionaries ordered increasingly by their time
            field. Each dictionary has at least three entries:
                - task_id: string representing the id of the task to which 
                    the event corresponds.
                - time: long representing a time stamp in epoch format.
                - state: a value corresponding to a task valid state. represents
                    the state to which a task may have changed.
        """
        if state and  not state in Task.VALID_STATES:
            raise ValueError("Provided state {} is not a valid "
                             "Task state".format(state))
        if (epoch_start!=None and epoch_start < 0 or
           epoch_end!=None and epoch_end < 0):
            raise ValueError("Time stamps cannot be negative values: {}, {}"
                             "".format(epoch_start, epoch_end))
        return self._events(epoch_start=epoch_start, epoch_end=epoch_end,
                            state=state)
        
    def burndown(self, epoch_start, epoch_end, bin_size):
        """
        Args:
            epoch_start: int, time stamp to start the burndown chart data.
                Value rounded down to seconds time stamp.
            epoch_end: int, time stamp to end the burndown char data. 
                Value rounded down to seconds time stamp.
            bin_size: int, time interval in seconds to group the counting. 
                
        Returns:
             A histogram-like dictionary including:
             - edges: list of epoch time stamps between each bin is constructed.
                It contains one more value that than the values.
             - values: a dictionary with one entry per valid task state (plus
                Total). Each entry is a list of the number of occurrences of the
                task state (id by the entry key) in each time interval (id by
                the position in the list).
        
        Exceptions raised.
            ValueError: if bin_size is not divisor of epoch_end-epoch_start.
        """
        epoch_start = int(epoch_start)
        epoch_end = int(epoch_end)
        bin_size = int(bin_size)
        
        if ((epoch_end-epoch_start)%bin_size!=0):
            raise ValueError("The desired time({0}) span must be multiple of "
                "bin_size({1})".format((epoch_end-epoch_start), bin_size))
        
        time_keys=list(range(epoch_start, epoch_end, bin_size))
        events = self.events(epoch_start, epoch_end)
        
        task_states=dict()
        summary = dict()
        summary['edges'] = time_keys +[epoch_end]
        summary['values'] = dict()
        values=summary['values']
        for task_state in Task.VALID_STATES+["Total"]:
            values[task_state] = list()
        for current_bin in time_keys:
            task_states[str(current_bin)]=dict()
        
        for (bin_time, next_bin_time) in zip(time_keys,
                                             (time_keys[1:]+
                                              [time_keys[-1]+bin_size])
                                             ):
            current_bin=str(bin_time)
            next_bin=str(next_bin_time)
            while (events and int(events[0]["time"])>=bin_time and
                   int(events[0]["time"])<next_bin_time):
                event=events[0]
                events.pop(0)
                task_states[current_bin][event["task_id"]] = event["state"]
                
            # We need before advancing
            task_states[next_bin] = dict(task_states[current_bin])            
            _count_and_append_task_states(task_states[current_bin], values)

        return summary

    #---------------------------------------------------------------------
    #- Backends should implement these methods; see the doc strings for
    #- the non-underscore versions of these functions for parameters.

    def _add(self, task, id=None, priority=None, requires=None):
        raise NotImplementedError
    
    #- Backend could override with more efficient implementation
    def _add_multiple(self, tasks, ids=None, priorities=None, requires=None):
        results = list()
        if ids is None:
            ids = [None,] * len(tasks)
        if priorities is None:
            priorities = [None,] * len(tasks)
        if requires is None:
            requires = [None,] * len(tasks)
            
        for i, task in enumerate(tasks):
            print("ADD", task, priorities[i], requires[i])
            results.append(self.add(task, id=ids[i], priority=priorities[i],
                                    requires=requires[i]))
        
        return results
    
    def _get(self, timeout=None, jobid=None):
        raise NotImplementedError 
         
    def _set_task_state(self, taskid, state, err=None, message=None):
        raise NotImplementedError

    def _retry(self, priority=None):
        raise NotImplementedError 

    def _rerun(self, priority=None):
        raise NotImplementedError 
     
    def _pause(self):
        raise NotImplementedError 
     
    def _resume(self):
        raise NotImplementedError 
     
    def _delete(self):
        raise NotImplementedError 

    def _state(self):
        raise NotImplementedError 

    def _tasks(self, id=None, state=None, summary=False):
        raise NotImplementedError 
    
    def _events(self, epoch_start, epoch_end, state=None):
        """Returns a list of of tasks state changes for the tasks in the queue
        between epoch_start and epoch_end. 
        
        Args:
            epoch_start: long representing the minimum epoch time stamp of an
                event.
            epoct_end: long representing the maximum epoch time stamp of an 
                event.
            state: If not set, it returns all events. If set to a valid task
                state, returns only the events corresponding to that state.
        
        Returns: A list of dictionaries ordered increasingly by their field
            time. Each dictionary has at least three entries:
                - task_id: string representing the id of the task to which 
                    the event corresponds.
                - time: long representing a time stamp in epoch format.
                - state: a value corresponding to a task valid state. represents
                    the state to which a task may have changed.
        """
        raise NotImplementedError

#-------------------------------------------------------------------------
#- Backends

#- Backends modules should implement:
#-   a subclass of qdo.Backend (see below)
#-   a subclass of qdo.Queue (see above)

#- qdo.Backend subclasses should implement __init__() with backend specific
#- parameters for how to connect to queues with this backend
#- (e.g. a directory for sqlite or a URL and token for a REST API).

#- Backend subclasses should also implement create(), connect(), and queues(),
#- but do not alter the call signature for these.  Once a user has a
#- Backend object, the create/connect/queues usage should be identical
#- across all backends.

class Backend(object):
    """Base class for qdo backends"""
    def __init__(self):
        """
        Returns a new Backend object
        
        Specific backends will add other input parameters as needed
        """
        raise NotImplementedError("Create a specific backend subclass, not the top level qdo.Backend")
        
    def create(self, queuename, user=None, worker=None):
        """
        Create a new queue and return a subclass of qdo.Queue
        
        Raises ValueError if queue_name already exists
        
        Optional inputs:
            user    : name of user who owns this queue (default $USER)
            worker  : name of worker connecting to this queue
        """
        raise NotImplementedError
        
    def connect(self, queuename, user=None, worker=None, create_ok=False):
        """
        Return a subclass of qdo.Queue representing queuename
        
        Optional inputs:
            user      : name of user who owns this queue (default $USER)
            worker    : name of worker connecting to this queue
            create_ok : if True, create queue if needed.  If False (default),
                        raise ValueError if queue doesn't already exist.
        """
        raise NotImplementedError
        
    def queues(self, user=None, worker=None):
        """
        Return a dict of known qdo.Queue objects, keyed by queuename

        Optional inputs:
            user    : name of user who owns this queue (default $USER)
            worker  : name of worker connecting to these queues
        """
        raise NotImplementedError

    #- For backwards compatibility; deprecated (?) and replaced with
    #- queues() which returns a dictionary
    def qlist(self, user=None):
        """Return a list of qdo.Queue objects for known queues"""
        return self.queues(user=user).values()
    
    def set_project(self, project):
        raise NotImplementedError("This backend does not support shared" +
                                   " project queues.")
    

# Backend --
# If QDO_BACKEND is set and is a string without a "." in it,
# assume it is a module in qdo.backends.
# Thus QDO_BACKEND='sqlite' is the default.
# If QDO_BACKEND has a "." in it, assume it is a fully-qualified module name.
# Either way, the name "qdoBackendClass" is loaded.

_backend = None

def _get_backend():
    global _backend
    if _backend is None:
        ### This would be easier with importlib, but that's only python2.7+
        backendname = os.environ.get('QDO_BACKEND', 'sqlite')
        if not '.' in backendname:
            backendname = 'qdo.backends.' + backendname
        #print('Loading module', backendname)
        mod = __import__(backendname, globals(), locals(), [], 0)
        # The __import__ of, eg, qdo.backends.postgres returns the *qdo* module,
        # not the "postgres" module.  Go up the package tree until we get the module
        # we actually specified.
        mods = backendname.split('.')
        mods.pop(0)
        while len(mods):
            submod = mods.pop(0)
            mod = getattr(mod, submod)
        # Now demand that the "qdoBackendClass" variable exists in that module, and run it.
        _backend = mod.qdoBackendClass()
    return _backend

#- Add qdo module level create(), connect(), and queues() functions that
#- use the default backend if the user is happy with that choice.

def create(queue_name, user=None, worker=None):
    """Connect to a pre-existing queue or create one if needed"""
    return _get_backend().create(queue_name, user=user, worker=worker)

def connect(queue_name, user=None, worker=None, create_ok=False):
    """Connect to a pre-existing Queue"""
    return _get_backend().connect(queue_name, user=user, worker=worker, create_ok=create_ok)

def queues(user=None):
    """Return dict of Queue objects for this user, keyed by queuename"""
    return _get_backend().queues(user)

#- DEPRECATED (?)
def qlist(user=None):
    """Return list of Queue objects for this user"""
    return _get_backend().qlist(user)

#-------------------------------------------------------------------------
#- Convenience and helper functions

def print_queues(user=None):
    """Print all active queues"""

    queues = _get_backend().queues(user).values()
    if len(queues) > 0:
        if user is None:
            print("%-20s %10s %8s" % ("QueueName", "User", "State"), end="")
        else:
            print("%-20s %8s" % ("QueueName", "State"), end="")
        print("%9s %9s %9s %9s %9s %9s" % ("Waiting", "Pending", "Running", "Succeeded", "Failed", "Killed"))
        for q in queues:
            try:
                cmd_count = q.status()['ntasks']
            except:
                print('%-20s ERROR' % q.name)
                continue
            if user is None:
                print("%-20s %10s %8s" % (q.name, q.user, q.state), end="")
            else:
                print("%-20s %8s" % (q.name, q.state), end="")

            print("%9d %9d %9d %9d %9d %9d" % (cmd_count[Task.WAITING],
                                                cmd_count[Task.PENDING],
                                                cmd_count[Task.RUNNING],
                                                cmd_count[Task.SUCCEEDED],
                                                cmd_count[Task.FAILED],
                                                cmd_count[Task.KILLED]))
    else:
        print("No known QDO queues")

def valid_queue_name(name):
    """
    Return True/False if name is a valid Queue name.

    [A-Za-z9-0_-] but no double-underscores allowed
    """
    ok = True
    if name.count('__') > 0:
        print("Sorry, double underscores are not allowed in queue names")
        ok = False
    # if name.count('-') > 0:
    #     print("Sorry, hyphens are not allowed in queue names")
    #     ok = False
    elif not name.replace('_', '').replace('-', '').isalnum():
        print("Sorry, queue names must be alphanumeric plus individual underscores and hyphens")
        ok = False
    
    return ok

def tojson(x):
    """
    Converts qdo objects to a JSON represenation.
    
    x can be:
      - Task
      - Queue
      - list or tuple of Tasks
      - list or tuple of Queues
      - dictionary of Queues keyed by Queue.name
      - dictionary of Tasks keyed by Task.id
    """
    if isinstance(x, (Task, Queue)):
        return json.dumps(dict(x))
    elif isinstance(x, (list, tuple)):
        if isinstance(x[0], Task):
            return json.dumps( {task.id:dict(task) for task in x} )
        elif isinstance(x[0], Queue):
            return json.dumps( {q.name:dict(q) for q in x} )
        else:
            raise ValueError("Can't JSON parse list of {}".format(type(x[0])))
    elif isinstance(x, dict):
        return json.dumps({key:dict(value) for (key, value) in x.items()})

def set_project(project):
    _get_backend().set_project(project)
    pass

def _qdo_dir():
    """Return $QDO_DIR, or if not set return location of qdo installation"""
    qd = os.environ.get('QDO_DIR', None)
    if qd is None:
        # parse /path/to/qdo/qdo/__init__.py -> /path/to/qdo
        qd = os.path.dirname(os.path.dirname(__file__))
    return qd

def _qdo_package_dir(package='qdo'):
    """Returns the location where the qdo python package is hosted."""
    loader = get_loader(package)
    if loader is None or not hasattr(loader, 'get_data'):
        return None
    mod = sys.modules.get(package) or loader.load_module(package)
    if mod is None or not hasattr(mod, '__file__'):
        return None
    return os.path.dirname(mod.__file__)

def _get_batch_config():
    """
    Read batch_config.ini file and return dictionary of parameters to use. It
    reads the profile from the environment, if not set it tries to infer
    the profile from the hostname.
    """
    cp = ConfigParser()
    cp.read(os.path.join(_qdo_package_dir(), 'etc', 'batch_config.ini'))
    profile = os.getenv('QDO_BATCH_PROFILE', None)
    if not profile is None:
        # Configured in enviroment, read that profile.
        config = dict(cp.items(profile))
    else:
        # Not configures, try first infering profile from hostname.
        config = _get_batch_config_from_hostname(cp)    
        if config is None:
            # Hostname didn't work, let's fall back to default.
            profile = cp.get('default', 'profile')
            config = dict(cp.items(profile))
    
    #- If cores_per_nodes isn't specified, guess based upon submission host
    if 'cores_per_node' not in config:
        from multiprocessing import cpu_count
        config['cores_per_node'] = cpu_count()
    else:
        config['cores_per_node'] = int(config['cores_per_node'])
    
    return config

def _get_batch_config_from_hostname(cp):
    """Returns a profile associated with the hostname. Returns none
    if profile cannot be found."""
    profile = _get_hostname_without_numbers()
    if profile is None:
            return None
    try:
        return dict(cp.items(profile))
    except NoSectionError:
        return None

def _get_hostname_without_numbers():
    """Retrieves hostname and removes numbers in case it is execution in a
    login node. e.g. if hostname is edison12.nersc.gov, it returns
    edison"""
    import socket
    import re

    hostname = socket.gethostname() 
    hostname = hostname.split(".")[0]
    m = re.search("\d", hostname)
    if m:
        return hostname[0:m.start()]
    if hostname == "":
        return None
    return hostname

def _count_and_append_task_states(tasks_dic, appendto):
    """Counts the numbers of tasks in each state and returns a dictionary with
    the result.

    Args:
        tasks_dic: dictionary of tasks in which the key correspond to the task
            id and the dict value to the task state.
    Returns: A dict in with the keys are all the possile Task states and the
        appendto the number of tasks that present each state. It includes an
        extra entry indexed by "Total" that contains the total number of tasks.
    """
    states = tasks_dic.values()
    summary=Counter(states)
    for state in Task.VALID_STATES:
        if not state in summary.keys():
            appendto[state].append(0)
        else:
            appendto[state].append(summary[state])
    appendto["Total"].append(len(tasks_dic))
    
    
#- Utility function to define default worker name
def _worker():
    import socket
    return socket.gethostname() + '-' + str(os.getpid())
        
def _check_valid(text):
    if text is None:
        return False
    text=text.strip()
    return text != " " and text != ""
    
def _parse_configuration(qdorc_route=None):
    """Parses the a qdorc file and configures qdo with its values). qdorc
    locations:
        - If qdorc_route is set, it will be read from the pointer rouce.
        - If not, it will try to read it from the environment variable 
          QDORC_LOCATION
        - If not set, it will try to read it from $QDO_DIR/qdorc
        - If QDO_DIR is not set, it will try to read it from ~/.qdo/qdorc
    
    If qdorc file is found, a sample qdorc is created at $QDO_DIR (if set) or
    ~/.qdo/  
    """ 

    standard_qdo_folder = os.getenv("QDO_DIR",os.path.join(
                                        os.path.expanduser("~"), '.qdo'))
    standard_qdorc_location=os.path.join(standard_qdo_folder, 'qdorc')

    if qdorc_route is None:
        qdorc_route=os.getenv("QDORC_LOCATION", standard_qdorc_location)
        
    if (os.path.exists(qdorc_route)):
        cp = ConfigParser()
        cp.read(qdorc_route)
        config = dict(cp.items("qdo"))
        for var_name in ["QDO_BATCH_PROFILE", "QDO_DB_DIR", "QDO_BACKEND",
                         "QDO_DIR", "QDO_DB_NAME", "QDO_DB_HOST",
                         "QDO_DB_PORT", "QDO_DB_USER", "QDO_DB_PASS"]:
            if var_name.lower() in config.keys():
                if not var_name in os.environ.keys():
                    value = config[var_name.lower()]
                    if (var_name in ["QDO_DIR",  "QDO_DB_DIR"] and
                        "~" in value):
                        value=os.path.expanduser(value)
                    os.environ[var_name] = value
    else:
        orig_qdorc=os.path.join(_qdo_package_dir(), 'etc', 'qdorc')
        dest_folder=os.path.dirname(qdorc_route)
        if not os.path.exists(dest_folder):
            os.makedirs(dest_folder)
        shutil.copyfile(orig_qdorc, qdorc_route)

_parse_configuration()



