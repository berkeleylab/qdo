/*
Example of the creation of SQL database the QDO postgresSQL backend:
- Database is named 'qdo'.
- Permisions to connect to the detabase are denied by default.
- Role qdo_connect is created, access to the qdo database is granted. User
  accounts will be gratned qdo_connect to access the qdo database.

This code can be used as an alternative to the method 
qdo.postgres.backends.postgres.AdminModule.create_new_db()

Naming convention: qdo_connect role would change accordingly if the
database name changes from 'qdo'.
*/
CREATE DATABASE qdo
WITH OWNER = gonzalo;

REVOKE CONNECT, TEMPORARY ON DATABASE qdo FROM public;

\connect qdo

REVOKE ALL ON SCHEMA public FROM public;

create role qdo_connect nologin;
grant connect on database qdo to qdo_connect;