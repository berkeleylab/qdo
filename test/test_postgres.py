"""UNIT TESTS for the class qdo.backends.postgres.AdminModule

Notes:
- It requires a running instance of PostgresSQL.
- It requires a user with admin level. Admin user and password care configured
  by setting the environment variables QDO_TEST_DB_USER, QDO_TEST_DB_PASSWORD.
  If not set it will using the unix username and no password.. 
- Database host, port, can be configured using environment variables:
  QDO_DB_HOST, QDO_DB_PORT. If not set it will connect to localhost on the
  5432 port.
- It requires that the db_user has capacity to created databases and grant
  permisions on them.

Sample use:

 python -m unittest test_postgress
 
Gonzalo P. Rodrigo (gprodrigoalvarez@lbl.gov), 2015
"""

from __future__ import print_function

import threading
import unittest
import os
from getpass import getuser

try:
    import psycopg2
    HAVE_PSYCOPG2 = True
except ImportError:
    HAVE_PSYCOPG2 = False

if HAVE_PSYCOPG2:  # this should be importable whenever we have psycopg2
    from qdo.backends.postgres import *

# Assume there's a postgres instance running if QDO_DB_HOST is defined.
HOSTNAME = os.environ.get("QDO_DB_HOST", None)

@unittest.skipIf((not HAVE_PSYCOPG2) or ("QDO_DB_HOST" not in os.environ),
                 "psycopg2 not installed or QDO_DB_HOST is not defined")
class TestAdminModule(unittest.TestCase):
    
    def setUp(self):
        self._host = os.environ.get('QDO_DB_HOST', '127.0.0.1')
        self._db_name = "unittest_db"
        self._port = os.environ.get('QDO_DB_PORT', 5432)
        self._user =  os.environ.get('QDO_TEST_DB_USER', getuser())
        self._password = os.environ.get('QDO_TEST_DB_PASS', None)
        self.addCleanup(self.db_destroy)
        self._admin = AdminModule(self._host, None, self._port,
                                  self._user, self._password)
        
        self.assertTrue(self._admin.create_new_db(self._db_name),
                        "Database creation failed.")
        
        self._db = self._admin._db

    def db_destroy(self):
        self._admin = AdminModule(self._host, None, self._port,
                                  self._user, self._password)
        self.assertTrue(self._admin.destroy_db(self._db_name),
                        "Database destruction failed.")
        pass
    
    def user_destroy(self, user):
        self.assertTrue(self._admin.delete_users([user]),
                       "Deleting user "+user)
    
    def create_connection(self, user_name, password):
        db = self._db.new()
        # Connecting as the user
        db.conn_dsn = dict(database=self._db_name, host=self._host,
                         port=self._port, user=user_name,
                         password=password)
        db=db.new()
        return db
    
    def test_create_destroy(self):
        pass

    def test_create_projects(self):
        projects = ["prj1", "prj2"]
        self.assertTrue(self._admin.create_projects(projects), 
                        "Creating projects "+str(projects))
        # These should not raise exception
        self._db.execute("SELECT * from prj1.tasks")
        self._db.execute("SELECT * from prj1.queues")
        self._db.execute("SELECT * from prj2.tasks")
        self._db.execute("SELECT * from prj2.queues")
        self._db.execute("INSERT INTO prj1.queues (name, owner, active)"+
                         " VALUES('q1','user', true)")
        self._db.execute("INSERT INTO prj1.tasks (task, state, priority)"+
                         " VALUES('t1','run!', 2.0)")
        # this should raise an Exception since prjFake does not exist.
        with self.assertRaises(Exception):
            self._db.execute("SELECT * from prjFake.queues")

    def test_create_users(self):
        self.addCleanup(self.user_destroy, "user_name")
        self.assertTrue(self._admin.create_user("user_name", "password"),
                        "Creating user user_name")
        db = self.create_connection("user_name", "password")
        
        db.execute("SELECT * from user_name.tasks")
        db.execute("SELECT * from user_name.queues")
        db.execute("SELECT * from tasks")
        db.execute("SELECT * from queues")

        self._admin.create_projects(["prj1"])
        with self.assertRaises(Exception):
            db.execute("SELECT * from prj1.queues")
    
    def test_grant_user_projects(self):
        self.addCleanup(self.user_destroy, "user_name")
        self.assertTrue(self._admin.create_user("user_name", "password"),
                        "Creating user user_name")
        
        db = self.create_connection("user_name", "password")
        projects=["prj1", "prj2"]
        self.assertTrue(self._admin.create_projects(projects))
        
        # Before granting
        with self.assertRaises(Exception):
            db.execute("SELECT * from prj1.queues")
            db.execute("SELECT * from prj2.queues")
        
        # After Granting
        self.assertTrue(self._admin.grant_user_projects("user_name",
                                                        projects),
                        "Error granting user_name access to "+str(projects))
        db.execute("SELECT * from prj1.queues")
        db.execute("SELECT * from prj2.queues")
        
        # After revoking
        self.assertTrue(self._admin.revoke_user_projects("user_name",
                                                         projects),
                        "Error revoking user_name access to "+str(projects))
        with self.assertRaises(Exception):
            db.execute("SELECT * from prj1.queues")
            db.execute("SELECT * from prj2.queues")

    def test_grant_project_users(self):
        self.addCleanup(self.user_destroy, "user_name1")
        self.assertTrue(self._admin.create_user("user_name1", "password"),
                        "Creating user user_name1")
        self.addCleanup(self.user_destroy, "user_name2")
        self.assertTrue(self._admin.create_user("user_name2", "password"),
                        "Creating user user_name2")

        self.assertTrue(self._admin.create_projects(["prj1"]))
        
        db1 = self.create_connection("user_name1", "password")
        db2 = self.create_connection("user_name2", "password")
        projects=["prj1", "prj2"]
        self._admin.create_projects(projects)
        
        # Before granting
        with self.assertRaises(Exception):
            db1.execute("SELECT * from prj1.queues")
            db2.execute("SELECT * from prj1.queues")
        
        # After granting
        self.assertTrue(self._admin.grant_project_users("prj1", ["user_name1",
                                                         "user_name2"]),
                        "Granting user_name1, user_name2 access to prj1 "+
                        "returned False.")
        db1.execute("SELECT * from prj1.queues")
        db2.execute("SELECT * from prj1.queues")
        
        # After revoking
        self.assertTrue(self._admin.revoke_project_users("prj1", ["user_name1",
                                                         "user_name2"]),
                        "Revoking user_name1, user_name2 access to prj1 "+
                        "returned False.")
        with self.assertRaises(Exception):
            db1.execute("SELECT * from prj1.queues")
            db2.execute("SELECT * from prj1.queues")
    
    def test_delete_projects(self):
        self.assertTrue(self._admin.create_projects(["prj1", "prj2"]),
                         "Error creating projects prj1, prj2")
        self._db.execute("SELECT * from prj1.queues")
        self._db.execute("SELECT * from prj2.queues")
        self.assertTrue(self._admin.delete_projects(["prj1", "prj2"]),
                         "Error creating projects prj1, prj2")
        with self.assertRaises(Exception):
            self._db.execute("SELECT * from prj1.queues")
            self._db.execute("SELECT * from prj2.queues")
     
    def test_enable_disable_users(self):   
        self.addCleanup(self.user_destroy, "user_name1")
        self.assertTrue(self._admin.create_user("user_name1", "password"),
                        "Creating user user_name1")
        self.addCleanup(self.user_destroy, "user_name2")
        self.assertTrue(self._admin.create_user("user_name2", "password"),
                        "Creating user user_name2")
        db1 = self.create_connection("user_name1", "password")
        db2 = self.create_connection("user_name2", "password")
        db1.retry_connect=False
        db2.retry_connect=False
        
        db1.execute("SELECT * from queues")
        db2.execute("SELECT * from queues")
        
        self.assertTrue(self._admin.disable_users(["user_name1", "user_name2"]), 
                       "Error disabling user")
         
        with self.assertRaises(Exception):
            db1.execute("SELECT * from queues")
            db2.execute("SELECT * from queues")
        
        self.assertTrue(self._admin.enable_users(["user_name1", "user_name2"]), 
                       "Error enabling user")
        db1.execute("SELECT * from queues")
        db2.execute("SELECT * from queues")
        
    def test_list_users(self):
        self.addCleanup(self.user_destroy, "user_name1")
        self.assertTrue(self._admin.create_user("user_name1", "password"),
                    "Creating user user_name1")
        self.addCleanup(self.user_destroy, "user_name2")
        self.assertTrue(self._admin.create_user("user_name2", "password"),
                            "Creating user user_name2")
        projects=["prj1", "prj2"]
        self._admin.create_projects(projects)
        self.assertTrue(self._admin.grant_project_users("prj1", ["user_name1",
                                                 "user_name2"]),
                "Granting user_name1, user_name2 access to prj1 "+
                "returned False.")
        self.assertTrue(self._admin.grant_project_users("prj2", ["user_name2"]),
                "Granting user_name2 access to prj1 "+
                "returned False.")
        
        self.assertTrue(self._admin.disable_users(["user_name2"]), 
                       "Error disabling user_name2")
        
        users, projects = self._admin._list_users_projects()
        
        #- Here we start the real checks
        #- Both users are there
        self.assertEqual(set(users.keys()), set(["user_name1", "user_name2"]),
             "Users listed don't match to created")
        #- List correct enable status
        self.assertTrue(users["user_name1"]["enabled"],
                       "user_name1 should be enabled")
        self.assertFalse(users["user_name2"]["enabled"],
                        "user_name2 should be disabled")
        #_ List correct projects 
        self.assertEqual(set(["user_name1", "prj1"]),
                         set(users["user_name1"]["projects"]),
                         "Projects for user_name1 do not match")
        self.assertEqual(set(["user_name2", "prj1", "prj2"]),
                         set(users["user_name2"]["projects"]),
                         "Projects for user_name2 do not match")
        
        #- Projects contain the correct user information
        #- Both projects are there.
        self.assertEqual(set(projects.keys()), set(["prj1", "prj2", 
                                                   "user_name1", "user_name2"]), 
                         "Projects listed don't match to created")
        #- Projects list the correct users.
        self.assertEqual(set(projects["prj1"]), set(["user_name1", 
                                                     "user_name2"]))
        self.assertEqual(set(projects["user_name1"]), set(["user_name1"]))
        self.assertEqual(set(projects["prj2"]), set(["user_name2"]))
        self.assertEqual(set(projects["user_name2"]), set(["user_name2"]))


@unittest.skipIf((not HAVE_PSYCOPG2) or ("QDO_DB_HOST" not in os.environ),
                 "psycopg2 not installed or QDO_DB_HOST is not defined")
class TestPostgres2(unittest.TestCase):
    
    
    
    def test_retry(self):
        backend = PostgresBackend()
    
        # Get and delete test queues to ensure that everything is empty.
        q1 = backend.connect('test1', create_ok=True)
        q1.delete()
        q1 = backend.connect('test1', create_ok=True)
        
        q2 = backend.connect('test2', create_ok=True)
        q2.delete()
        q2 = backend.connect('test2', create_ok=True)
    
        print('\nEmpty queues:')
        print('Q1 tasks:', q1.tasks())
        print('Q2 tasks:', q2.tasks())
        q1.print_tasks()
        q2.print_tasks()

        self.assertEqual(len(q1.tasks()), 0)
        self.assertEqual(len(q2.tasks()), 0)

        ta = q1.add('A')
        tb = q2.add('B')

        self.assertEqual(q1.tasks()[0].state, Task.PENDING)
        self.assertEqual(q2.tasks()[0].state, Task.PENDING)

        q1.set_task_state(ta, Task.FAILED)

        self.assertEqual(q1.tasks()[0].state, Task.FAILED)
        self.assertEqual(q2.tasks()[0].state, Task.PENDING)

        q2.set_task_state(tb, Task.FAILED)

        self.assertEqual(q1.tasks()[0].state, Task.FAILED)
        self.assertEqual(q2.tasks()[0].state, Task.FAILED)
        
        print('\nAfter adding tasks and setting to FAILED:')
        print('Q1 tasks:', q1.tasks())
        print('Q2 tasks:', q2.tasks())
        print('Q1:')
        q1.print_tasks()
        print('Q2:')
        q2.print_tasks()

        self.assertEqual(len(q1.tasks()), 1)
        self.assertEqual(len(q2.tasks()), 1)

        print('\nQ1.retry()')

        q1.retry()

        print('\nAfter q1.retry():')
        print('Q1 tasks:', q1.tasks())
        print('Q2 tasks:', q2.tasks())
        q1.print_tasks()
        q2.print_tasks()
        
        self.assertEqual(len(q1.tasks()), 1)
        self.assertEqual(len(q2.tasks()), 1)

        self.assertEqual(q1.tasks()[0].state, Task.PENDING)
        self.assertEqual(q2.tasks()[0].state, Task.FAILED)

        # Test _recover()
        q2.set_task_state(tb, Task.RUNNING)

        print('\nAfter setting task B to RUNNING:')
        print('Q1 tasks:', q1.tasks())
        print('Q2 tasks:', q2.tasks())
        q1.print_tasks()
        q2.print_tasks()

        self.assertEqual(len(q2.tasks()), 1)
        self.assertEqual(q2.tasks()[0].state, Task.RUNNING)

        q2.recover()

        print('\nQ2.recover():')
        print('Q1 tasks:', q1.tasks())
        print('Q2 tasks:', q2.tasks())
        q1.print_tasks()
        q2.print_tasks()
        
        self.assertEqual(len(q2.tasks()), 1)
        self.assertEqual(q2.tasks()[0].state, Task.PENDING)
        self.assertEqual(q1.tasks()[0].state, Task.PENDING)


@unittest.skipIf((not HAVE_PSYCOPG2) or ("QDO_DB_HOST" not in os.environ),
                 "psycopg2 not installed or QDO_DB_HOST is not defined")
class PostgresStressTestCase(unittest.TestCase):
    def run_mp(self, i):
        done = []
        while True:
            backend = PostgresBackend()
            q = backend.connect('testqueue', create_ok=True)
            task = q.get(timeout=1)
            print('thread', i, 'got', task)
            if task is None:
                break
            done.append(task)
        return done

    def run_threading(self, i):
        while True:
            backend = PostgresBackend()
            q = backend.connect('testqueue', create_ok=True)
            task = q.get(timeout=1)
            print('thread', i, 'got', task)
            if task is None:
                self.donelock.acquire()
                self.cleanfinish.append(i)
                self.donelock.release()
                break
            time.sleep(1)
            self.donelock.acquire()
            self.alldone.append(task)
            self.donelock.release()

    def test_stress(self):
        self.donelock = threading.Lock()
        self.alldone = []
        self.cleanfinish = []

        # Following code makes sure that we use a db for test, and not
        # whatever the user has configured.
        user = getuser()
        db_admin = AdminModule("localhost", '', None, 
                               os.getenv("QDO_TEST_DB_USER", user),
                               os.getenv("QDO_TEST_DB_PASS", ''))
        # Cleanup in case the script didnt end before.
        db_admin.destroy_db("unittest_db")
        db_admin.delete_users(["unittest_qdo"])

        db_admin.create_new_db("unittest_db")
        db_admin.create_user("unittest_qdo", "fakepass")
        os.environ["QDO_DB_NAME"] = "unittest_db"
        os.environ["QDO_DB_USER"] = "unittest_qdo"
        os.environ["QDO_DB_PASS"] = "fakepass"
        os.environ["QDO_DB_PORT"] = "5432" 
        os.environ["QDO_DB_HOST"] = "localhost" 


        backend = PostgresBackend()

        q = backend.connect('testqueue', create_ok=True)
        q.db.execute('DELETE FROM tasks WHERE queue_id=%s', (q.qid,))
        print('Queuing tasks...')
        T = 10000
        q.add_multiple(['task%i' % i for i in range(T)])

        if True:
            N = 10000
            threads = []
            for i in range(N):
                try:
                    t = threading.Thread(target=self.run_threading,
                                         args=(i,))
                    t.start()
                    threads.append(t)
                except:
                    print('Failed to create thread', i)
                    break

            print('joining')
            for i,t in enumerate(threads):
                print('Joining thread', i, t)
                t.join()

            print('Total of', len(self.alldone), 'tasks done')
            print(len(self.cleanfinish), 'of', len(threads),
                  'threads finished cleanly')

        if False:
            import multiprocessing
            N = 90
            pool = multiprocessing.Pool(N)
            D = pool.map(self.run_mp, range(N))
            done = []
            for d in D:
                done.extend(d)
            print('Total number of tasks done:', len(done))

        db_admin.delete_users(["unittest_qdo"])
        db_admin.destroy_db("unittest_db")
