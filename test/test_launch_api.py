"""UNIT TESTS for the parameters calculators used in launch. It does not
require external components.

It does not test launch.

 python -m unittest test_launch_api
 
"""

import unittest
import os
import qdo
from qdo.launch_calc import (calculate_mpi_worker_parameters,
                             calculate_non_mpi_worker_parameters,
                             QDOLaunchException)

class TestLaunch(unittest.TestCase):
    def test_calculate_mpi_case1(self):
        # Cases one core per PE, setting only cores_per_pe 
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=1,
                        cores_per_mpiproc=1,
                        cores_per_worker=None,
                        numa_pack=False,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker, 1)
        self.assertEqual(cores_per_pe, 1) 
        self.assertEqual(pes_per_node, 1)
        self.assertEqual(pes_per_numa, None)  
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=12,
                        cores_per_mpiproc=1,
                        cores_per_worker=None,
                        numa_pack=False,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker, 12)
        self.assertEqual(cores_per_pe, 1) 
        self.assertEqual(pes_per_node, 12)
        self.assertEqual(pes_per_numa, None) 
        
        # cores_per_pe > 1 setting only cores_per_pe
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=12,
                        cores_per_mpiproc=4,
                        cores_per_worker=None,
                        numa_pack=False,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 48)
        self.assertEqual(pes_per_worker, 12)
        self.assertEqual(cores_per_pe, 4) 
        self.assertEqual(pes_per_node, 6)
        self.assertEqual(pes_per_numa, None) 
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=12,
                        cores_per_mpiproc=7,
                        cores_per_worker=None,
                        numa_pack=False,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 4*24)
        self.assertEqual(pes_per_worker, 12)
        self.assertEqual(cores_per_pe, 7) 
        self.assertEqual(pes_per_node, 3)
        self.assertEqual(pes_per_numa, None) 
        
    def test_calculate_mpi_case1_numa(self):
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=1,
                        cores_per_mpiproc=1,
                        cores_per_worker=None,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker, 1)
        self.assertEqual(cores_per_pe, 1) 
        self.assertEqual(pes_per_node, 1)
        self.assertEqual(pes_per_numa, 1)
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=12,
                        cores_per_mpiproc=1,
                        cores_per_worker=None,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker, 12)
        self.assertEqual(cores_per_pe, 1) 
        self.assertEqual(pes_per_node, 12)
        self.assertEqual(pes_per_numa, 6) 
        
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=12,
                        cores_per_mpiproc=4,
                        cores_per_worker=None,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 48)
        self.assertEqual(pes_per_worker, 12)
        self.assertEqual(cores_per_pe, 4) 
        self.assertEqual(pes_per_node, 6)
        self.assertEqual(pes_per_numa, 3) 
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=12,
                        cores_per_mpiproc=7,
                        cores_per_worker=None,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 6*24)
        self.assertEqual(pes_per_worker, 12)
        self.assertEqual(cores_per_pe, 7) 
        self.assertEqual(pes_per_node, 2)
        self.assertEqual(pes_per_numa, 1)

    def test_calculate_mpi_case2(self):
        # setting cores_per_worker
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=1,
                        cores_per_mpiproc=None,
                        cores_per_worker=24,
                        numa_pack=False,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker, 1)
        self.assertEqual(cores_per_pe, 24) 
        self.assertEqual(pes_per_node, 1)
        self.assertEqual(pes_per_numa, None)
    
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=3,
                        cores_per_mpiproc=None,
                        cores_per_worker=24,
                        numa_pack=False,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker,3)
        self.assertEqual(cores_per_pe, 8) 
        self.assertEqual(pes_per_node, 3)
        self.assertEqual(pes_per_numa, None)
        
    def test_calculate_mpi_case2_numa(self):
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=1,
                        cores_per_mpiproc=None,
                        cores_per_worker=24,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker, 1)
        self.assertEqual(cores_per_pe, 12) 
        self.assertEqual(pes_per_node, 1)
        self.assertEqual(pes_per_numa, 1)
            
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=7,
                        cores_per_mpiproc=None,
                        cores_per_worker=24,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker,7)
        self.assertEqual(cores_per_pe, 3) 
        self.assertEqual(pes_per_node, 7)
        self.assertEqual(pes_per_numa, 4)
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=3,
                        cores_per_mpiproc=None,
                        cores_per_worker=24,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker,3)
        self.assertEqual(cores_per_pe, 6) 
        self.assertEqual(pes_per_node, 3)
        self.assertEqual(pes_per_numa, 2)
        
        # Resulting cores per PE cannot be more than a node
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
            calculate_mpi_worker_parameters(
                        mpiproc_per_worker=1,
                        cores_per_mpiproc=None,
                        cores_per_worker=48,
                        numa_pack=False,
                        cores_per_node=24, cores_per_numa=12)

        self.assertEqual(final_cores_per_worker, 48)
        self.assertEqual(pes_per_worker,1)
        self.assertEqual(cores_per_pe, 24) 
        self.assertEqual(pes_per_node, 1)
        self.assertEqual(pes_per_numa, None)
        
        # Resulting cores per PE cannot be more than a NUMA
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
            calculate_mpi_worker_parameters(
                        mpiproc_per_worker=1,
                        cores_per_mpiproc=None,
                        cores_per_worker=24,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker,1)
        self.assertEqual(cores_per_pe, 12) 
        self.assertEqual(pes_per_node, 1)
        self.assertEqual(pes_per_numa, 1)
        
    def test_calculate_mpi_case3(self):    
        # Now setting both cores_per_pe and cores_per_worker
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=3,
                        cores_per_mpiproc=3,
                        cores_per_worker=24,
                        numa_pack=False,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker,3)
        self.assertEqual(cores_per_pe, 3) 
        self.assertEqual(pes_per_node, 3)
        self.assertEqual(pes_per_numa, None)

    def test_calculate_mpi_case3_numa(self):    
        # Now setting both cores_per_pe and cores_per_worker
        
        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=3,
                        cores_per_mpiproc=3,
                        cores_per_worker=24,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 24)
        self.assertEqual(pes_per_worker,3)
        self.assertEqual(cores_per_pe, 3) 
        self.assertEqual(pes_per_node, 3)
        self.assertEqual(pes_per_numa, 2)

        final_cores_per_worker, pes_per_worker, cores_per_pe, \
                pes_per_node, pes_per_numa = \
        calculate_mpi_worker_parameters(
                        mpiproc_per_worker=3,
                        cores_per_mpiproc=3,
                        cores_per_worker=48,
                        numa_pack=True,
                        cores_per_node=24, cores_per_numa=12)
        self.assertEqual(final_cores_per_worker, 48)
        self.assertEqual(pes_per_worker,3)
        self.assertEqual(cores_per_pe, 3) 
        self.assertEqual(pes_per_node, 2)
        self.assertEqual(pes_per_numa, 1)

    def test_calculate_mpi_case3_exceptions(self): 
        """fewer cores per PE than cores_per_pe should error"""
        self.assertRaises(QDOLaunchException, calculate_mpi_worker_parameters,
                          mpiproc_per_worker=8,
                          cores_per_mpiproc=4,
                          cores_per_worker=24,
                          numa_pack=True,
                          cores_per_node=24, cores_per_numa=12)     
    
    def test_calculate_mpi_exceptions(self):
        """0 cores_per_pe or 0 cores_per_worker should error"""
        self.assertRaises(QDOLaunchException, calculate_mpi_worker_parameters,
                          mpiproc_per_worker=8,
                          cores_per_mpiproc=0,
                          cores_per_worker=24,
                          numa_pack=True,
                          cores_per_node=24, cores_per_numa=12)


        self.assertRaises(QDOLaunchException, calculate_mpi_worker_parameters,
                          mpiproc_per_worker=8,
                          cores_per_mpiproc=2,
                          cores_per_worker=0,
                          numa_pack=True,
                          cores_per_node=24, cores_per_numa=12)
        
        # cores_per_pe larger than cores_per_numa
        self.assertRaises(QDOLaunchException, calculate_mpi_worker_parameters,
                          mpiproc_per_worker=8,
                          cores_per_mpiproc=13,
                          cores_per_worker=None,
                          numa_pack=True,
                          cores_per_node=24, cores_per_numa=12)

        # cores_per_pe is larger than cores_per_node
        self.assertRaises(QDOLaunchException, calculate_mpi_worker_parameters,
                          mpiproc_per_worker=8,
                          cores_per_mpiproc=25,
                          cores_per_worker=None,
                          numa_pack=False,
                          cores_per_node=24, cores_per_numa=12)

        # cores_per_worker is not multiple of cores_per_node
        self.assertRaises(QDOLaunchException, calculate_mpi_worker_parameters,
                          mpiproc_per_worker=8,
                          cores_per_mpiproc=None,
                          cores_per_worker=25,
                          numa_pack=False,
                          cores_per_node=24, cores_per_numa=12)

        
    def test_calculate_non_mpi(self):
        total_cores, pes_per_node, pes_per_numa  = \
            calculate_non_mpi_worker_parameters(
                                         nworkers=24,
                                         cores_per_worker=1,
                                         numa_pack=False,
                                         cores_per_node=24,
                                         cores_per_numa=12)  
        self.assertEqual(total_cores, 24)
        self.assertEqual(pes_per_node, 24)
        self.assertEqual(pes_per_numa, None) 
        
        total_cores, pes_per_node, pes_per_numa  = \
                    calculate_non_mpi_worker_parameters(
                                         nworkers=36,
                                         cores_per_worker=1,
                                         numa_pack=False,
                                         cores_per_node=24,
                                         cores_per_numa=12)  
        self.assertEqual(total_cores, 48)
        self.assertEqual(pes_per_node, 24)
        self.assertEqual(pes_per_numa, None) 
        
        
        total_cores, \
                pes_per_node, pes_per_numa  = \
                    calculate_non_mpi_worker_parameters(
                                         nworkers=24,
                                         cores_per_worker=2,
                                         numa_pack=False,
                                         cores_per_node=24,
                                         cores_per_numa=12)  
        self.assertEqual(total_cores, 48)
        self.assertEqual(pes_per_node, 12)
        self.assertEqual(pes_per_numa, None) 
        
        total_cores, \
                pes_per_node, pes_per_numa  = \
                    calculate_non_mpi_worker_parameters(
                                         nworkers=10,
                                         cores_per_worker=3,
                                         numa_pack=False,
                                         cores_per_node=24,
                                         cores_per_numa=12)  
        self.assertEqual(total_cores, 48)
        self.assertEqual(pes_per_node, 8)
        self.assertEqual(pes_per_numa, None) 
        
        

              
    def test_calculate_non_mpi_numa(self):  
        total_cores, \
                pes_per_node, pes_per_numa  = \
                    calculate_non_mpi_worker_parameters(
                                         nworkers=20,
                                         cores_per_worker=5,
                                         numa_pack=True,
                                         cores_per_node=24,
                                         cores_per_numa=12)  
        self.assertEqual(total_cores, 120)
        self.assertEqual(pes_per_node, 4)
        self.assertEqual(pes_per_numa, 2)   
        
    def test_calculate_non_mpi_exceptions(self): 
        self.assertRaises(QDOLaunchException,
                          calculate_non_mpi_worker_parameters,
                          nworkers=20,
                          cores_per_worker=13,
                          numa_pack=True,
                          cores_per_node=24,
                          cores_per_numa=12)

        # cores_per_worker not a multiple of cores_per_node
        self.assertRaises(QDOLaunchException,
                          calculate_non_mpi_worker_parameters,
                          nworkers=20,
                          cores_per_worker=25,
                          numa_pack=False,
                          cores_per_node=24,
                          cores_per_numa=12)
