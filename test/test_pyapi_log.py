"""UNIT TESTS for logging code of the class qdo.backensd

Notes:
- It requires a well configured backend
- For postgres testing: requires a running postgress db, and that the
  unix user has admin permissions over it.  

Sample use:

 python -m unittest test_pyapi_log
 
"""

import calendar
import qdo
import os
import shutil
import time
import unittest

from qdo import Task
from qdo.backends.postgres import AdminModule
from getpass import getuser

# TODO: split apart sqlite/postgres backend tests, so that both are run.

class TestPyAPIQDO(unittest.TestCase):
    def setUp(self):
        backend_choice = os.environ.get('QDO_BACKEND');
        if not backend_choice or backend_choice == 'sqlite':
            self.setUp_sqlite()
        elif backend_choice=='postgres':
            self.setUp_postgres()
        else:
            raise Exception("Unknown back-end, cannot test")

        self.q = qdo.create('TestBlatFoo')

    def tearDown(self):
        self.q.delete()

    def setUp_sqlite(self):
        self._db_dir="./testdb/"
        os.environ["QDO_DB_DIR"] =  self._db_dir
        self.addCleanup(self.sqlite_destroy)
        
    def sqlite_destroy(self):
        if (self._db_dir):
            shutil.rmtree(self._db_dir)
            
    def setUp_postgres(self):
        self._host = os.environ.get('QDO_DB_HOST', "localhost")
        self._db_name = "unittest_db"
        self._port = 5432
        self._user = getuser()
        self._password = os.environ.get('QDO_DB_PASS', None)
        
        self._admin = AdminModule(self._host, None, self._port,
                                  self._user, self._password)
        
        self.addCleanup(self.postgress_destroy)
        self.assertTrue(self._admin.create_new_db(self._db_name),
                        "Database creation failed.")
      
        os.environ["QDO_DB_NAME"] =  self._db_name
        self._db = self._admin._db
        
        self.assertTrue(self._admin.create_user("user_name", "password"),
                        "Creating user user_name")
        self.addCleanup(self.user_destroy, "user_name")
        os.environ["QDO_DB_USER"] =  "user_name"
        os.environ["QDO_DB_PASS"] =  "password"

    def postgress_destroy(self):
        self._admin = AdminModule(self._host, None, self._port,
                                  self._user, self._password)
        self.assertTrue(self._admin.destroy_db(self._db_name),
                        "Database destruction failed.")

    def user_destroy(self, user):
        self.assertTrue(self._admin.delete_users([user]),
                       "Deleting user "+user)

    def test_all_events_pending(self):
        self.q.add("exit 0")
        self.q.add("exit 1")

        events = self.q.events()

        self.assertEqual(len(events), 2)                
        self.assertEqual(events[0]["state"], Task.PENDING)
        self.assertEqual(events[1]["state"], Task.PENDING)
        
    def now(self):
        return calendar.timegm(time.gmtime())
        return int(time.time())
    
    def test_one_task_life(self):
        self.q.add("exit 0")
        
        events = self.q.events()
        
        self.assertEqual(len(events), 1)
        self.assertEqual(events[0]["state"], Task.PENDING)
        
        self.q.do(timeout=0)
        events = self.q.events()
        self.assertEqual(len(events), 3)
        self.assertEqual(events[0]["state"], Task.PENDING)
        self.assertEqual(events[1]["state"], Task.RUNNING)
        self.assertEqual(events[2]["state"], Task.SUCCEEDED)
    
    def test_one_failed_task_life(self):   
        self.q.add("exit 1")
       
        events = self.q.events()
        
        self.assertEqual(len(events), 1)
        self.assertEqual(events[0]["state"], Task.PENDING)
        
        self.q.do(timeout=0)
        events = self.q.events()
        self.assertEqual(len(events), 3)
        self.assertEqual(events[0]["state"], Task.PENDING)
        self.assertEqual(events[1]["state"], Task.RUNNING)
        self.assertEqual(events[2]["state"], Task.FAILED)
        self.assertEqual(events[2]["err"], 1)
        
    
    def test_two_tasks_life(self):
        task_id = self.q.add("sleep 1")
        dep_id = self.q.add("exit 2", requires=task_id)
        
        events = self.q.events()
        
        self.assertEqual(len(events), 2)
        self.assertEqual(events[0]["state"], Task.PENDING)
        self.assertEqual(events[1]["state"], Task.WAITING)
        
        self.q.do(runtime=1)
        events = self.q.events()
        self.assertEqual(len(events), 5)
        self.assertEqual(events[0]["state"], Task.PENDING)
        self.assertEqual(events[1]["state"], Task.WAITING)
        self.assertEqual(events[2]["state"], Task.RUNNING)
        self.assertEqual(events[3]["state"], Task.SUCCEEDED)
        self.assertEqual(events[4]["state"], Task.PENDING)
        
        self.assertEqual(events[4]["task_id"], dep_id)

        self.q.do(timeout=0)
        events = self.q.events()
        self.assertEqual(len(events), 7)
        self.assertEqual(events[0]["state"], Task.PENDING)
        self.assertEqual(events[1]["state"], Task.WAITING)
        self.assertEqual(events[2]["state"], Task.RUNNING)
        self.assertEqual(events[3]["state"], Task.SUCCEEDED)
        self.assertEqual(events[4]["state"], Task.PENDING)
        self.assertEqual(events[5]["state"], Task.RUNNING)
        self.assertEqual(events[6]["state"], Task.FAILED)
    
    def test_timing(self): 
        time_before_create = self.now()
        time.sleep(1)
        self.q.add("sleep 1")
        time.sleep(1)
        time_after_create = self.now()
        time.sleep(1)
        self.q.do(timeout=0)
        
        events = self.q.events(epoch_end=time_before_create)
        # There should be no events
        self.assertEqual(len(events), 0)
        
        events = self.q.events(epoch_start=time_before_create)
        # There should be no events
        self.assertEqual(len(events), 3)
        
        events = self.q.events(epoch_start=time_before_create, 
                               epoch_end=time_after_create)
        # There should be no events
        self.assertEqual(len(events), 1)
        
    def test_task_type(self): 
        task_id = self.q.add("sleep 1")
        dep_id = self.q.add("exit 2", requires=task_id)
        
        events = self.q.events()
        
        self.assertEqual(len(events), 2)
                
        self.q.do(timeout=0)
        events = self.q.events()
        self.assertEqual(len(events), 7)
        self.assertEqual(events[0]["state"], Task.PENDING)
        self.assertEqual(events[1]["state"], Task.WAITING)
        self.assertEqual(events[2]["state"], Task.RUNNING)
        self.assertEqual(events[3]["state"], Task.SUCCEEDED)
        self.assertEqual(events[4]["state"], Task.PENDING)
        self.assertEqual(events[5]["state"], Task.RUNNING)
        self.assertEqual(events[6]["state"], Task.FAILED)
        
        events = self.q.events(state=Task.PENDING)
        self.assertEqual(len(events), 2)
        
        events = self.q.events(state=Task.WAITING)
        self.assertEqual(len(events), 1)
        
        events = self.q.events(state=Task.SUCCEEDED)
        self.assertEqual(len(events), 1)


    def test_task_types_time(self):
        time_before_create = self.now()
        task_id = self.q.add("sleep 1")
        dep_id = self.q.add("exit 2", requires=task_id)
        time.sleep(1)

        time_after_create = self.now()
        
        events = self.q.events()
        self.assertEqual(len(events), 2)

        self.q.do(runtime=1)
        time_after_do = self.now()
        time.sleep(1)
        self.q.do(timeout=0)

        events = self.q.events(state=Task.RUNNING)
        self.assertEqual(len(events), 2)

        events = self.q.events(epoch_start=time_after_create, 
                               epoch_end=time_after_do, state=Task.RUNNING)
        self.assertEqual(len(events), 1)
        
    def task_exceptions(self):
        with self.assertRaises(ValueError):
            self.q.events(epoch_start=-1)
        
        with self.assertRaises(ValueError):
            self.q.events(epoch_end=-1)
            
        with self.assertRaises(ValueError):
            self.q.events(state="bad value")
            
        with self.assestRaise(ValueError):
            self.q.burndown(0, 10, 3)
    
    def test_burndown(self):
        time_before_create = self.now() - 1
        
        task_id = self.q.add("sleep 3")
        dep_id = self.q.add("sleep 1; exit 2", requires=task_id)
        time.sleep(2)

        self.q.do(timeout=0)
        time_after_do = self.now() + 2
        
        bd= self.q.burndown(time_before_create, time_after_do, 1)
        
        for task_type in qdo.Task.VALID_STATES + ['Total']:
            self.assertEqual(len(bd['edges']),
                             len(bd['values'][task_type]) + 1)

        # No tasks at the begining
        total = bd['values']['Total']
        running = bd['values'][Task.RUNNING]
        pending = bd['values'][Task.PENDING]
        waiting = bd['values'][Task.WAITING]
        succeded = bd['values'][Task.SUCCEEDED]
        failed = bd['values'][Task.FAILED]
        
        self.assertEqual(total[0], 0)
        
        # After a second 2 tasks, 1 pending, 1 waiting
        
        self.assertEqual(total[1], 2)
        self.assertEqual(pending[1], 1)
        self.assertEqual(waiting[1], 1)        

        self.assertEqual(total[3], 2)
        self.assertEqual(running[3], 1)
        self.assertEqual(waiting[3], 1)
        
        self.assertEqual(total[4], 2)
        self.assertEqual(running[4], 1)
        self.assertEqual(waiting[4], 1)
        
        self.assertEqual(total[-3], 2)
        self.assertEqual(running[-3], 1)
        self.assertEqual(succeded[-3], 1)  
        
        self.assertEqual(total[-1], 2)
        self.assertEqual(succeded[-1], 1)
        self.assertEqual(failed[-1], 1)
