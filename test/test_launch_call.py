"""UNIT TESTS for launch calls to check ir parapmeters arrive to qsub/aprun
(or equivalent) correctly.

It uses a mock Super computer interface with scripts emulating qsub and 
aprun locally. This scripts register the received parameters that are checked
after execution of each launch. It covers most of the functions in launch but
"script" and "runtime".  

Sample use:

 python -m unittest test_launch_call
 
"""

from __future__ import print_function

import os
import shutil
import unittest

import qdo
from qdo import Task

class TestLaunchCall(unittest.TestCase):
    def setUp(self):
        self._db_dir="/tmp/qdo_testdb/"
        os.makedirs(self._db_dir)
        os.environ["QDO_DB_DIR"] =  self._db_dir
        self.addCleanup(self.sqlite_destroy)        
        self._test_dir="/tmp/qdo_test_launch/"
        self.addCleanup(self.remove_test_dir)
        if not os.path.exists(self._test_dir):
            os.makedirs(self._test_dir)
        os.environ["QDO_TEST_DIR"]=self._test_dir
        os.environ["QDO_BATCH_PROFILE"]="test-profile"
        
        current_dir = os.path.dirname(os.path.realpath(__file__));
        old_path=os.getenv("PATH", None)
        os.environ["PATH"]=old_path+":"+current_dir
        
        self._q=qdo.create("testq")

    def tearDown(self):
        self._q.delete()

    def sqlite_destroy(self):
        if (self._db_dir):
            shutil.rmtree(self._db_dir)
            
    def remove_test_dir(self):
        if (self._test_dir):
            shutil.rmtree(self._test_dir)

    def _read_qsub_params(self):
        qsub_file=open(self._test_dir+"qsub_mock.out", "r") 
        lines = qsub_file.readlines()
        values = dict()
        for line in lines:
            parts = line.split("|")
            key=parts[0]
            if (len(parts)>1):
                value ="|".join(parts[1:])
            else:
                value=None
            if key in ["jobname", "batchqueue"]:
                values[key]=value.strip()
            elif key == "ncoreswall":
                sub_values_list=value.strip().split(" ")
                for sub_value in sub_values_list:
                    sub_value=sub_value.split("=")
                    key=sub_value[0]
                    if (key=="mppwidth"):
                        values["ncores"]=sub_value[-1]
                    elif (key=="walltime"):
                        values["walltime"]=sub_value[-1]
                    
            elif key == "rest_params":
                sub_values=value.strip().split()
                values["script"]=sub_values[-1]
                values["batchopts"]=" ".join(sub_values[:-1])
        qsub_file.close()
        return values

    def _read_apprun_files(self):
        file_names = os.listdir(self._test_dir)
        time_stamps= []
        single_file=None
        for name in sorted(file_names):
            if not single_file:
                single_file=name
            if "aprun_mock" in name:
                words = name.split(".")
                stamp=words[2]
                if not stamp in time_stamps:
                    time_stamps.append(stamp) 
        return time_stamps, self._read_apprun_file_params(self._test_dir+
                                                          single_file)
        
    
    def _read_apprun_file_params(self, file_route):
        aprun_file=open(file_route, "r") 
        lines = aprun_file.readlines()
        values=dict()
        for line in lines:
            line=line.strip()
            words=line.split("|")
            key=words[0]
            value=None
            if len(words):
                value="|".join(words[1:])
            if (key =="qdo_call"):
                values["task"]=value
            else:
                values[key] = value
            
        aprun_file.close()
        return values
    
    def _get_max_dif(self, stamps):
        """Returns maximum difference in timestamps. Used to avoid flackyness
        in time tests."""
        if len(stamps)==1:
            return 0
        return int(stamps[-1])-int(stamps[0])

    def test_non_mpi_sequential(self):
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.launch(1, njobs=1, mpiproc_per_worker=0,
                       cores_per_mpiproc=None, 
                       cores_per_worker=1,
                       numa_pack=False, script=None, timeout=0,
                       runtime=None,
                       batchqueue="myqueue",
                       walltime="00:02:00",
                       batch_opts='-A account', verbose=True,
                       export_all_env_vars=True)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], "testq")
        self.assertEqual(qsub_values["ncores"], "24")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution 
        self.assertGreaterEqual(self._get_max_dif(time_stamps), 0)
        
        self.assertEqual(len(time_stamps), 1)
        self.assertEqual(apprun_params["nPEs"],"1")
        self.assertEqual(apprun_params["PEs_per_node"],"1")
        self.assertEqual(apprun_params["cores_per_PE"],"1")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"0")
        self.assertEqual(apprun_params["task"],"qdo do testq")
        print(apprun_params)
        self.assertGreaterEqual(int(apprun_params["epoch_end"])-
                                int(apprun_params["epoch_start"]),4)
        self._read_apprun_files()
        
    def test_non_mpi_parallel(self):
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.launch(2, njobs=1, mpiproc_per_worker=0,
                       cores_per_mpiproc=None, 
                       cores_per_worker=24,
                       numa_pack=False, script=None, timeout=0,
                       runtime=None,
                       batchqueue="myqueue",
                       walltime="00:02:00",
                       batch_opts='-A account', verbose=True)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], "testq")
        self.assertEqual(qsub_values["ncores"], "48")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Parallel execution
        self.assertLessEqual(self._get_max_dif(time_stamps), 1)
        self.assertEqual(apprun_params["nPEs"],"2")
        self.assertEqual(apprun_params["PEs_per_node"],"1")
        self.assertEqual(apprun_params["cores_per_PE"],"24")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"0")
        self.assertEqual(apprun_params["task"],"qdo do testq")
        self._read_apprun_files()
    
    def test_non_mpi_dustin(self):
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        
        self._q.launch(16, njobs=1, mpiproc_per_worker=0,
                       cores_per_mpiproc=None, 
                       cores_per_worker=6,
                       numa_pack=False, script=None, timeout=0,
                       runtime=None,
                       batchqueue="myqueue",
                       walltime="00:02:00",
                       batch_opts='-A account', verbose=True)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], "testq")
        self.assertEqual(qsub_values["ncores"], "96")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Parallel execution
        self.assertLessEqual(self._get_max_dif(time_stamps), 1)
        self.assertEqual(apprun_params["nPEs"],"16")
        self.assertEqual(apprun_params["PEs_per_node"],"4")
        self.assertEqual(apprun_params["cores_per_PE"],"6")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"0")
        self.assertEqual(apprun_params["task"],"qdo do testq")
        self.assertGreaterEqual(int(apprun_params["epoch_end"])-
                        int(apprun_params["epoch_start"]),4)
        self._read_apprun_files()

    def test_mpi_sequential(self):
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.launch(1, njobs=1, mpiproc_per_worker=4,
                       cores_per_mpiproc=12, 
                       cores_per_worker=None,
                       numa_pack=False, script=None, timeout=0,
                       runtime=None,
                       batchqueue="myqueue",
                       walltime="00:02:00",
                       batch_opts='-A account', verbose=True,
                       export_all_env_vars=True)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], "testq")
        self.assertEqual(qsub_values["ncores"], "48")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution
        self.assertGreaterEqual(self._get_max_dif(time_stamps), 2)
        
        self.assertEqual(apprun_params["nPEs"],"4")
        self.assertEqual(apprun_params["PEs_per_node"],"2")
        self.assertEqual(apprun_params["cores_per_PE"],"12")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"0")
        self.assertEqual(apprun_params["task"],"sleep 2")
        self._read_apprun_files()
    
    def test_mpi_parallel(self):
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.launch(2, njobs=1, mpiproc_per_worker=4,
                       cores_per_mpiproc=12, 
                       cores_per_worker=None,
                       numa_pack=False, script=None, timeout=0,
                       runtime=None,
                       batchqueue="myqueue",
                       walltime="00:02:00",
                       batch_opts='-A account', verbose=True,
                       export_all_env_vars=True)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], "testq")
        self.assertEqual(qsub_values["ncores"], "96")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution
        self.assertLessEqual(self._get_max_dif(time_stamps), 1)

        self.assertEqual(apprun_params["nPEs"],"4")
        self.assertEqual(apprun_params["PEs_per_node"],"2")
        self.assertEqual(apprun_params["cores_per_PE"],"12")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"0")
        self.assertEqual(apprun_params["task"],"sleep 2")
        self._read_apprun_files()
     
    def test_non_mpi_parallel_numa(self):
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.launch(3, njobs=1, mpiproc_per_worker=0,
                       cores_per_mpiproc=None, 
                       cores_per_worker=8,
                       numa_pack=True, script=None, timeout=0,
                       runtime=None,
                       batchqueue="myqueue",
                       walltime="00:02:00",
                       batch_opts='-A account', verbose=True,
                       export_all_env_vars=True)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], "testq")
        self.assertEqual(qsub_values["ncores"], "48")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution 
        self.assertLessEqual(self._get_max_dif(time_stamps), 1)

        self.assertEqual(apprun_params["nPEs"],"3")
        self.assertEqual(apprun_params["PEs_per_node"],"2")
        self.assertEqual(apprun_params["cores_per_PE"],"8")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"1")
        self.assertEqual(apprun_params["task"],"qdo do testq")
        self._read_apprun_files()    
        
    def test_mpi_parallel_numa(self):
        self._q.add("sleep 2")
        self._q.add("sleep 2")
        self._q.launch(2, njobs=1, mpiproc_per_worker=3,
                       cores_per_mpiproc=8, 
                       cores_per_worker=None,
                       numa_pack=True, script=None, timeout=0,
                       runtime=None,
                       batchqueue="myqueue",
                       walltime="00:02:00",
                       batch_opts='-A account', verbose=True,
                       export_all_env_vars=True)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], "testq")
        self.assertEqual(qsub_values["ncores"], "96")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution
        self.assertLessEqual(self._get_max_dif(time_stamps), 1)

        self.assertEqual(apprun_params["nPEs"],"3")
        self.assertEqual(apprun_params["PEs_per_node"],"2")
        self.assertEqual(apprun_params["cores_per_PE"],"8")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"1")
        self.assertEqual(apprun_params["task"],"sleep 2")
        self._read_apprun_files()

    def test_multiword_script(self):
        import tempfile
        import stat
        with tempfile.NamedTemporaryFile() as temp1, tempfile.NamedTemporaryFile()  as temp2:

            self._q.add("A")

            scriptfn = temp1.name
            outfn = temp2.name
            os.chmod(scriptfn, stat.S_IWUSR | stat.S_IXUSR | stat.S_IRUSR)
            temp1.write('echo B $* > %s' % outfn)
            temp1.flush()

            self._q.launch(1, njobs=1, mpiproc_per_worker=0,
                           cores_per_mpiproc=None,
                           cores_per_worker=1,
                           numa_pack=False, script='%s C' % scriptfn, timeout=0,
                           runtime=None,
                           batchqueue="myqueue",
                           walltime="00:02:00",
                           batch_opts='-A account', verbose=True)
        
            qsub_values = self._read_qsub_params()
            print('multiword qsub values:', qsub_values)
            self.assertEqual(qsub_values["jobname"], "testq")
            self.assertEqual(qsub_values["ncores"], "24")
            self.assertEqual(qsub_values["walltime"], "00:02:00")
            self.assertEqual(qsub_values["batchqueue"], "myqueue")
            self.assertEqual(qsub_values["batchopts"], "-A account")
        
            time_stamps, apprun_params=self._read_apprun_files()
            print('Apprun params:', apprun_params)
            self.assertEqual(apprun_params["nPEs"],"1")
            self.assertEqual(apprun_params["PEs_per_node"],"1")
            self.assertEqual(apprun_params["cores_per_PE"],"1")
            self.assertEqual(apprun_params["PEs_per_numa_node"],"0")
            self.assertEqual(apprun_params["task"],"qdo do testq")
            self._read_apprun_files()

            txt = open(outfn, 'r').read()
            print('Read text: "%s"' % txt)
            self.assertEqual(txt, 'B C A\n')

