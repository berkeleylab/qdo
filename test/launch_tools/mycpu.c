/*
 * C lib encapsulate the system call sched_getcpu().
 * This call only exists in Linux.
 *
 * Created to be imported in Fortran 90
 */
#include <utmpx.h>
int sched_getcpu();

int findmycpu_ ()
{
    int cpu;
    cpu = sched_getcpu();
    return cpu;
}
