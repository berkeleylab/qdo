#!/usr/bin/env python
# This is a test program to run on a multiprocessor system to check if
# multiple processes run on different processors. It launches n processes
# and prints the CPU executing each one of them. 
#
# Requires cpumodule to be compiled: Read compile_python_tools.sh
#
# Usage:
#     cpu_test n
# Arguments:
#     n: integer greater than 0 that indicates the number of processes to
#     launch.



import multiprocessing as mp
import sys
import time
import cpumodule

def process_action(proces_id):
    time.sleep(1)
    processor_id=cpumodule.getmycpu()
    print("Ole! from {0} at processor {1}".format(proces_id,processor_id))

args_ok = True

if len(sys.argv) < 2:
    args_ok = False
else:
    try:
        num_processes = int(sys.argv[1])
    except:
        args_ok = False

if not args_ok:
    print("Missing argument: number of threads to run.")
    print("Usage: cpu_test n")
    print("Arguments: n integer greater than 0 that indicates the number" 
          "of processes to launch")
    exit(-1)

process_list = []
print("Launching {0} processes".format(num_processes,))

for i in range(num_processes):
    new_process = mp.Process(target=process_action, args=(i,))
    process_list.append(new_process)
    new_process.start()

print("Waiting for {0} processes to end".format(len(process_list)))
for p in process_list:
    p.join()
    print("Process ended!")

print("All processes ended")



    

    





